package com.community.core.config;

import java.util.Properties;

import org.broadleafcommerce.common.email.service.info.EmailInfo;
import org.broadleafcommerce.common.email.service.message.MessageCreator;
import org.broadleafcommerce.common.email.service.message.NullMessageCreator;
import org.broadleafcommerce.presentation.thymeleaf3.email.BroadleafThymeleaf3MessageCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.thymeleaf.TemplateEngine;
/**
 * Shared email configuration
 * 
 * @author Phillip Verheyden (phillipuniverse)
 */
@Configuration
public class CoreEmailConfig {

    /**
     * A dummy mail sender has been set to send emails for testing purposes only
     * To view the emails sent use "DevNull SMTP" (download separately) with the following setting: 
     *   Port: 30000
     */
    @Bean
    public JavaMailSender blMailSender() {
    	JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        Properties mailProperties = new Properties();
        mailProperties.put("mail.smtp.auth", false);
        mailProperties.put("mail.smtp.starttls.enable", false);
        mailSender.setJavaMailProperties(mailProperties);
        mailSender.setHost("10.25.69.191");
        mailSender.setPort(25);
        mailSender.setProtocol("smtp");
        mailSender.setUsername("onegri@mitrphol.com");
        mailSender.setPassword("p@ssw0rd");
        return mailSender;
    }
    /**
     * Uncomment this bean to send real emails
     */
    @Bean
    @Autowired
    public MessageCreator blMessageCreator(@Qualifier("blEmailTemplateEngine") TemplateEngine tlTemplateEngine, @Qualifier("blMailSender") JavaMailSender mailSender) {
        return new BroadleafThymeleaf3MessageCreator(tlTemplateEngine, mailSender);
    }
    
//    @Bean
//    @Autowired
//    public MessageCreator blMessageCreator(@Qualifier("blMailSender") JavaMailSender mailSender) {
//        return new NullMessageCreator(mailSender);
//    }
    
    @Bean
    public EmailInfo blEmailInfo() {
        EmailInfo info = new EmailInfo();
        info.setFromAddress("itcane@mitrphol.com");
        info.setSendAsyncPriority("2");
        info.setSendEmailReliableAsync("false");
        return info;
    }
    
    @Bean
    public EmailInfo blRegistrationEmailInfo() {
        EmailInfo info = blEmailInfo();
        info.setSubject("You have successfully registered!");
        info.setEmailTemplate("register-email");
        return info;
    }
    
    @Bean
    public EmailInfo blForgotPasswordEmailInfo() {
        EmailInfo info = blEmailInfo();
        info.setSubject("Reset password request");
        info.setEmailTemplate("resetPassword-email");
        return info;
    }
    
    @Bean
    public EmailInfo blOrderConfirmationEmailInfo() {
        EmailInfo info = blEmailInfo();
        info.setSubject("Your order with Mitr Phol Market Place");
        info.setEmailTemplate("orderConfirmation-email");
        return info;
    }
    
    @Bean
    public EmailInfo blFulfillmentOrderTrackingEmailInfo() {
        EmailInfo info = blEmailInfo();
        info.setSubject("Your order with Mitr Phol Market Place Has Shipped");
        info.setEmailTemplate("fulfillmentOrderTracking-email");
        return info;
    }
    
    @Bean
    public EmailInfo blReturnAuthorizationEmailInfo() {
        EmailInfo info = blEmailInfo();
        info.setSubject("Your return with Mitr Phol Market Place");
        info.setEmailTemplate("returnAuthorization-email");
        return info;
    }
    
    @Bean
    public EmailInfo blReturnConfirmationEmailInfo() {
        EmailInfo info = blEmailInfo();
        info.setSubject("Your return with Mitr Phol Market Place");
        info.setEmailTemplate("returnConfirmation-email");
        return info;
    }
}

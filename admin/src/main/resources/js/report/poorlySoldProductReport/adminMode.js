let maxData = 10;
let isAdmin = false;
let isFacAdmin = false;
let defaultFacCode = ''
$(document).ready(function () {
    console.log('best seller report listView')
    checkIsAdmin()
    setComponentScreen()

    $('#btn_export').on('click',function () {

        for(let i=0; i<$('input[name="selector"]').length; i++){
            if($('input[name="selector"]')[i].checked == true){
                maxData = $('input[name="selector"]')[i].value
                break;
            }
        }

        console.log('maxData  = '+maxData)
        let facCodeCompare = isFacAdmin === true ? defaultFacCode:$('#search_by_fac').val()
        let param = '' ;
        param += '?limitMax='+maxData;
        param += '&facCode='+facCodeCompare
        param += '&year='+$('#search_by_year option:selected').val() ;
        param += '&manFac='+$('#search_creator').val() ;

        window.location.href = window.location.origin+$('#contextRoot').val()+'report/poorlySoldProductReport/generateReportAdmin'+param

    })
    renderListYearSearch()
})


function renderListYearSearch() {

    let d = new Date();
    let currentYear = d.getFullYear();
    for(let i=-10; i<10; i++){
        if(parseInt(currentYear+i) == parseInt(currentYear)){
            $('#search_by_year').append('' +
                '<option selected value="'+(currentYear+i)+'">'+(currentYear+i)+'</option>' +
                '')
        }else{
            $('#search_by_year').append('' +
                '<option value="'+(currentYear+i)+'">'+(currentYear+i)+'</option>' +
                '')
        }

    }
    $('#btn_export').attr('disabled',false)
    $('#btn_export').removeClass('not-allow-button')
    $('.items').css('height','100%')

}


function checkIsAdmin(){
    let sizeRole = $('.sub-role-style').length
    for(let v=0; v<sizeRole; v++){
        if ($('.sub-role-style')[v].innerHTML == 'ROLE_ADMIN' ){
            isAdmin = true;
        }else if($('.sub-role-style')[v].innerHTML == 'ROLE_FACTORY_ADMIN'){
            isFacAdmin = true;
            defaultFacCode = ($('#facCode_User').val().split('FACTORY_CODE=')[1].split(',')[0]).replace('}','')
        }
    }

}
function setComponentScreen() {
    if(isAdmin == true){
        $('#div_search_fac').removeClass('hide-block')
    }else{
        $('#div_search_fac').addClass('hide-block')
    }

}
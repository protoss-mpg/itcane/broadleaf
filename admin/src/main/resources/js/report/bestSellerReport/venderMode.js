let maxData = 10;
let manufacture = $('#user_manufac').text()
$(document).ready(function () {
    console.log('best seller report listView')

    $('#btn_export').on('click',function () {

        for(let i=0; i<$('input[name="selector"]').length; i++){
            if($('input[name="selector"]')[i].checked == true){
                maxData = $('input[name="selector"]')[i].value
                break;
            }
        }

        console.log('maxData  = '+maxData)
        let param = '' ;
        param += '?limitMax='+maxData;
        param += '&year='+$('#search_by_year option:selected').val() ;
        param += '&manFac='+manufacture

        window.location.href = window.location.origin+$('#contextRoot').val()+'report/bestSellerReport/generateReportVender'+param

    })
    renderListYearSearch()
})

function renderListYearSearch() {

    let d = new Date();
    let currentYear = d.getFullYear();
    for(let i=-10; i<10; i++){
        if(parseInt(currentYear+i) == parseInt(currentYear)){
            $('#search_by_year').append('' +
                '<option selected value="'+(currentYear+i)+'">'+(currentYear+i)+'</option>' +
                '')
        }else{
            $('#search_by_year').append('' +
                '<option value="'+(currentYear+i)+'">'+(currentYear+i)+'</option>' +
                '')
        }

    }
    $('#btn_export').attr('disabled',false)
    $('#btn_export').removeClass('not-allow-button')
    $('.items').css('height','100%')

}
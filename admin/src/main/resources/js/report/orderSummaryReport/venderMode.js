let manufacture = $('#user_manufac').text()
$(document).ready(function () {
    console.log('report order summary listView Vender')

    $('#btn_export').on('click',function () {

        console.log('btn_export order summary !')
        let param = '' ;
            param += '?year='+$('#search_by_year option:selected').val() ;
           param += '&manufac='+manufacture
        console.log('param = '+param)
        window.location.href = window.location.origin+$('#contextRoot').val()+'report/orderSummaryReport/generateReportVender'+param
    })

    renderListYearSearch()
})

function renderListYearSearch() {

    let d = new Date();
    let currentYear = d.getFullYear();
    for(let i=-10; i<10; i++){
        if(parseInt(currentYear+i) == parseInt(currentYear)){
            $('#search_by_year').append('' +
                '<option selected value="'+(currentYear+i)+'">'+(currentYear+i)+'</option>' +
                '')
        }else{
            $('#search_by_year').append('' +
                '<option value="'+(currentYear+i)+'">'+(currentYear+i)+'</option>' +
                '')
        }

    }

    $('#btn_export').attr('disabled',false)
    $('#btn_export').removeClass('not-allow-button')

}

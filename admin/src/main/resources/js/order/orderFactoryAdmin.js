let objSearch = {
    orderNum : '',
    facCode : '',
    customer : '',
    status : '',
    stAmount : '',
    edAmount : '',
    stDate : '',
    edDate : '',
}
let firstMaxCount = true
let urlSite = window.location.origin + $('#order_detail_url').text();
let isAdmin = false;
let isFacAdmin = false;
let defaultFacCode = $('#facCode_User').text()
let username_login = $('#user_login').text()
$(document).ready(function () {
    console.log('order factory admin !!!')
        checkIsAdmin()
        setComponentScreen()
        setDataCriteria()
        findOrderByCriteria()

    $('#btn_search').on('click',function () {
        if(setDataCriteria())   {
            console.log('search order')
            findOrderByCriteria()
        }
    })

    $('#search_amount_st').keypress(function(e) {
        let ipKey = e.which
        if(!(ipKey == 46 || (ipKey >= 48 && ipKey <= 57))){
            return false;
        }else{
            console.log($('#search_amount_ed').val())
        }
    });

    $('#search_amount_ed').keypress(function(e) {
        let ipKey = e.which
        if(!(ipKey == 46 || (ipKey >= 48 && ipKey <= 57))){
            return false;
        }else{
            console.log($('#search_amount_ed').val())
        }
    });

    $('#search_amount_st').on('blur',function () {
        if($('#search_amount_st').hasClass('number-format-wrong')){
            if($.isNumeric($('#search_amount_st').val())){
                $('#search_amount_st').removeClass('number-format-wrong')
            }
        }
    })

    $('#search_amount_ed').on('blur',function () {
        if($('#search_amount_ed').hasClass('number-format-wrong')){
            if($.isNumeric($('#search_amount_ed').val())){
                $('#search_amount_ed').removeClass('number-format-wrong')
            }
        }
    })

    setTimeout(function () {
        $('.listgrid-body-wrapper').addClass('hide-block')
    })


});

function setDataCriteria() {
    if(validateNumberField()){
        objSearch['facCode'] = defaultFacCode
        objSearch['orderNum'] = $('#search_by_order_num').val()
        objSearch['customer'] = $('#search_order_customer').val()
        objSearch['stAmount'] = $('#search_amount_st').val()
        objSearch['status'] = $('#search_order_status').val()
        objSearch['edAmount'] = $('#search_amount_ed').val()
        objSearch['stDate'] = $('#search_date_st').val()
        objSearch['edDate'] = $('#search_date_ed').val()
        return true;
    }else{
        return false
    }

}

function findOrderByCriteria() {

	var contextRoot = $('#contextRoot').val();
	
    $.ajax({
        type: "GET",
        url: window.location.origin+contextRoot+'orderCustom/findOrderbyCriteriaRoleFactoryAdmin' ,
        async:false,
        data : {
            facCode : objSearch['facCode'],
            orderNum : objSearch['orderNum'],
            customer : objSearch['customer'],
            status : objSearch['status'],
            amountSt : objSearch['stAmount'],
            amountEd : objSearch['edAmount'],
            dateSt : objSearch['stDate'],
            dateEd : objSearch['edDate']
        },
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    if(xhr.responseText){
                        let obj = JSON.parse(xhr.responseText);
                        let orderList = obj.data
                        console.log('orderList = '+orderList)
                        if(firstMaxCount){
                            $('#maxSizeOrder').text(orderList.length)
                            firstMaxCount=false
                        }
                        $('#bodyOrder').empty()
                        if(orderList.length > 0){
                            let contentBody = ''
                            for(let i =0; i<orderList.length; i++){
                                contentBody += ''+
                                    '<tr style="background: '+(i%2 ==0 ? '#F9F7F3' : '#F1EFEB')+' !important;">' +
                                    '<th title="Click to see order '+orderList[i].orderNumber+' detail" scope="col" class="explicit-size">' +
                                    '<div href="#" class="listgrid-headerBtn split dropdown">' +
                                    '<div class="listgrid-title" style="font-weight: normal; cursor: pointer !important;">' +
                                    '&#160;&#160;&#160;&#160;<span onclick="orderDetail(\''+orderList[i].orderNumber+'\')" style="color: #809831; text-decoration: underline; cursor: pointer;">'+''+orderList[i].orderNumber+'</span>' +
                                    '</div>' +
                                    '</div>' +
                                    '</th>' +
                                    '<th scope="col" class="explicit-size">' +
                                    '<div href="#" class="listgrid-headerBtn split dropdown">' +
                                    '<div class="listgrid-title" style="font-weight: normal;  ">' +
                                    ' <span>'+orderList[i].customer.username+'</span>' +
                                    '</div>' +
                                    '</div>' +
                                    '</th>' +
                                    '<th scope="col" class="explicit-size">' +
                                    '<div href="#" class="listgrid-headerBtn split dropdown">' +
                                    '<div class="listgrid-title" style="font-weight: normal; ">' +
                                    ' <span>'+nvl(orderList[i].name)+'</span>' +
                                    '</div>' +
                                    '</div>' +
                                    '</th>' +
                                    '<th scope="col" class="explicit-size">' +
                                    '<div href="#" class="listgrid-headerBtn split dropdown">' +
                                    '<div class="listgrid-title" style="font-weight: normal; ">' +
                                    ' <span>'+(orderList[i].submitDate).split(' ')[0]+'</span>' +
                                    '</div>' +
                                    '</div>' +
                                    '</th>'+
                                    '<th scope="col" class="explicit-size">' +
                                    '<div href="#" class="listgrid-headerBtn split dropdown">' +
                                    '<div class="listgrid-title" style="font-weight: normal; ">' +
                                    ' <span>'+(orderList[i].status.type)+'</span>' +
                                    '</div>' +
                                    '</div>' +
                                    '</th>'+
                                    '<th scope="col" class="explicit-size">' +
                                    '<div href="#" class="listgrid-headerBtn split dropdown">' +
                                    '<div class="listgrid-title" style="font-weight: bold; text-align: left">' +
                                    '&#160;&#160;&#160; <span>'+orderList[i].total.amount+'</span>' +
                                    '</div>' +
                                    '</div>' +
                                    '</th>';


                                if((orderList[i].status.type) == 'WAIT'){
                                    contentBody += '<th style="text-align: center;" scope="col" class="explicit-size">' +
                                        '<div href="#" class="listgrid-headerBtn split dropdown">' +
                                        '<div class="listgrid-title" style="font-weight: normal; ">' +
                                        '<button onclick="approveOrder(\''+orderList[i].orderNumber+'\')" title="Approve Order" style="background: green; color: white; cursor: pointer;"><i class="fa fa-check" aria-hidden="true"></i>\nApprove</button>' +
                                        '<button onclick="rejectOrder(\''+orderList[i].orderNumber+'\')" title="Reject Order" style="background: red; color: white; cursor: pointer;"><i class="fa fa-times" aria-hidden="true"></i>Reject</button>' +
                                        '</div>' +
                                        '</div>' +
                                        '</th>';
                                }else{
                                    contentBody+= '<th style="text-align: center;" scope="col" class="explicit-size">' +
                                    '<div href="#" class="listgrid-headerBtn split dropdown">' +
                                    '<div class="listgrid-title" style="font-weight: normal; ">' +
                                    '<span></span>'
                                    '</div>' +
                                    '</div>' +
                                    '</th>';
                                }

                                contentBody += '</tr>';
                            }
                            $('#bodyOrder').append(contentBody)
                            $('#firstResult').text('1')
                            $('#lastResult').text(orderList.length)
                            $('#maxResult').text(orderList.length)
                            $('#blockPaging').removeClass('hide-block')
                            $('#blockPaging').addClass('show-block')

                        }else{
                            $('#blockPaging').removeClass('show-block')
                            $('#blockPaging').addClass('hide-block')
                            let contentBody = ''
                            contentBody+=  '<tr style="background: '+(0%2 ==0 ? '#F9F7F3' : '#F1EFEB')+' !important;">' +
                                '<th  scope="col" class="explicit-size">' +
                                '<div href="#" class="listgrid-headerBtn split dropdown">' +
                                '<div class="listgrid-title" style="font-weight: normal;">' +
                                '&#160;&#160;&#160;&#160;<span>'+''+'(No records found)'+'</span>' +
                                '</div>' +
                                '</div>' +
                                '</th>' +
                                '<th scope="col" class="explicit-size">' +
                                '<div href="#" class="listgrid-headerBtn split dropdown">' +
                                '<div class="listgrid-title" style="font-weight: normal;  ">' +
                                '</div>' +
                                '</div>' +
                                '</th>' +
                                '<th scope="col" class="explicit-size">' +
                                '<div href="#" class="listgrid-headerBtn split dropdown">' +
                                '<div class="listgrid-title" style="font-weight: normal; ">' +
                                '</div>' +
                                '</div>' +
                                '</th>' +
                                '<th scope="col" class="explicit-size">' +
                                '<div href="#" class="listgrid-headerBtn split dropdown">' +
                                '<div class="listgrid-title" style="font-weight: normal; ">' +
                                '</div>' +
                                '</div>' +
                                '</th>'+
                                '<th scope="col" class="explicit-size">' +
                                '<div href="#" class="listgrid-headerBtn split dropdown">' +
                                '<div class="listgrid-title" style="font-weight: bold; text-align: left">' +
                                '</div>' +
                                '</div>' +
                                '</th>' +
                                '<th scope="col" class="explicit-size">' +
                                '<div href="#" class="listgrid-headerBtn split dropdown">' +
                                '<div class="listgrid-title" style="font-weight: bold; text-align: left">' +
                                '</div>' +
                                '</div>' +
                                '</th>' +
                                '<th scope="col" class="explicit-size">' +
                                '<div href="#" class="listgrid-headerBtn split dropdown">' +
                                '<div class="listgrid-title" style="font-weight: bold; text-align: left">' +
                                '</div>' +
                                '</div>' +
                                '</th>' +
                                '</tr>';

                            $('#bodyOrder').append(contentBody)
                        }
                    }else{
                        console.log('NOT FOUND')
                    }

                }
            }
        }
    }).done(function (){
        //close loader
    });
}

function orderDetail(orderNum){
    let urlOrder = urlSite+orderNum
    window.open(
        urlOrder
    )
}

function validateNumberField() {
    let count = 0;
    if(!$.isNumeric($('#search_amount_st').val()) && !$('#search_amount_st').val() == '' ){
        $('#search_amount_st').addClass('number-format-wrong')
        count++;
    }
    if(!$.isNumeric($('#search_amount_ed').val()) && !$('#search_amount_ed').val() == ''){
        $('#search_amount_ed').addClass('number-format-wrong')
        count++
    }

    if(count ==0)
        return true;
    else
        return false;
}

function checkIsAdmin(){
    let sizeRole = $('.sub-role-style').length
    for(let v=0; v<sizeRole; v++){
        if ($('.sub-role-style')[v].innerHTML == 'ROLE_ADMIN' ){
            isAdmin = true;
         }else if($('.sub-role-style')[v].innerHTML == 'ROLE_FACTORY_ADMIN'){
            isFacAdmin = true;
        }
    }

}
function setComponentScreen() {
    if(isAdmin == true){
        $('#search_by_fac').removeClass('hide-block')
    }else{
        $('#search_by_fac').addClass('hide-block')
    }

}

function approveOrder(orderNum){
    console.log('approve order : '+orderNum)
    let statusApprove = 'APPROVE';
    var contextRoot = $('#contextRoot').val();

    $.ajax({
        type: "GET",
        url: window.location.origin+contextRoot+'orderCustom/changeStatusOrder/orderNumber/'+orderNum+'/status/'+statusApprove ,
        async:false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    if(xhr.responseText){
                        let obj = JSON.parse(xhr.responseText);
                        let responseMessage = obj.success
                        console.log('responseMessage = '+responseMessage)
                        alert('Approve order '+orderNum+' success')
                        if(setDataCriteria())   {
                            console.log('search order')
                            findOrderByCriteria()
                        }
                    }else{
                        console.log('Fail to update status')
                    }

                }
            }
        }
    }).done(function (){
        //close loader
    });
}

function rejectOrder(orderNum){
    console.log('approve order : '+orderNum)
    let statusReject = 'REJECT';
    var contextRoot = $('#contextRoot').val();

    $.ajax({
        type: "GET",
        url: window.location.origin+contextRoot+'orderCustom/changeStatusOrder/orderNumber/'+orderNum+'/status/'+statusReject ,
        async:false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    if(xhr.responseText){
                        let obj = JSON.parse(xhr.responseText);
                        let responseMessage = obj.success
                        console.log('responseMessage = '+responseMessage)
                        alert('Reject order '+orderNum+' success')
                        if(setDataCriteria())   {
                            console.log('search order')
                            findOrderByCriteria()
                        }
                    }else{
                        console.log('Fail to update status')
                    }

                }
            }
        }
    }).done(function (){
        //close loader
    });
}

function nvl(str) {
    if(str == null){
        return '';
    }else{
        return str;
    }
}


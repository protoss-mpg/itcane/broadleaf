package com.community.admin.web.repository;

import com.community.admin.web.model.ResponseModel;

public interface OrderRepositoryCustom {

    ResponseModel changeStatusOrder(String orderNumber, String status);
}

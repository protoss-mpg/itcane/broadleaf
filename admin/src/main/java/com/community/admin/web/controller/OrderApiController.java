package com.community.admin.web.controller;

import com.community.admin.web.model.ResponseModel;
import com.community.admin.web.service.OrderCustomService;
import com.protosstechnology.commons.util.JSONUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.broadleafcommerce.core.order.domain.Order;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/orderCustom")
public class OrderApiController {
    private static final Log LOG = LogFactory.getLog(ClearTempFileController.class);

    @Resource(name = "blOrderCustomService")
    OrderCustomService orderCustomService;

    @GetMapping(value="/findOrderbyCriteriaRoleAdmin",produces = "application/json; charset=utf-8")
    public ResponseModel findOrderbyCriteriaRoleAdmin(
            @RequestParam(value = "facCode", required = false)String facCode,
            @RequestParam(value = "orderNum", required = false)String orderNum,
            @RequestParam(value = "customer", required = false)String customer,
            @RequestParam(value = "status", required = false)String status,
            @RequestParam(value = "amountSt", required =  false)String amountSt,
            @RequestParam(value = "amountEd", required = false)String amountEd,
            @RequestParam(value = "dateSt", required = false)String dateSt,
            @RequestParam(value = "dateEd", required =  false)String dateEd

    ){

        ResponseModel responseModel = new ResponseModel();
        responseModel.setData(new HashMap<>());
        try {
            List<Order> orderList = orderCustomService.findOrderbyCriteriaRoleAdmin(facCode,orderNum,customer,status,amountSt,amountEd,dateSt,dateEd);
            String jsonReturn  = (JSONUtil.getJSONSerializer()
                    .include("id")
                    .include("submitDate")
                    .include("orderNumber")
                    .include("total.amount")
                    .include("subTotal.amount")
                    .include("status")
                    .include("status.*")
                    .include("orderStatus")
                    .include("orderStatus.*")
                    .include("orderStatus.friendlyType")
                    .include("orderStatus.type")
                    .include("name")
                    .include("customer.mainEntityName")
                    .include("customer.username")
                    .exclude("*")
                    .serialize(orderList));

            responseModel.setSuccess(true);
            responseModel.setData(JSONUtil.listFromJson(jsonReturn));

            if(orderList!=null && !orderList.isEmpty()) {
                responseModel.setMessage("Order "+orderList.size()+" records");
            }else {
                responseModel.setMessage("No Data Found");
            }

        } catch (Exception e) {
            LOG.error("Exception ");
            responseModel.setSuccess(false);
            responseModel.setMessage(e.getMessage());
            e.printStackTrace();
        }

        return responseModel;
    }

    @GetMapping(value="/findOrderbyCriteriaRoleFactoryAdmin",produces = "application/json; charset=utf-8")
    public ResponseModel findOrderbyCriteriaRoleFactoryAdmin(
            @RequestParam(value = "facCode", required = false)String facCode,
            @RequestParam(value = "orderNum", required = false)String orderNum,
            @RequestParam(value = "customer", required = false)String customer,
            @RequestParam(value = "status", required = false)String status,
            @RequestParam(value = "amountSt", required =  false)String amountSt,
            @RequestParam(value = "amountEd", required = false)String amountEd,
            @RequestParam(value = "dateSt", required = false)String dateSt,
            @RequestParam(value = "dateEd", required =  false)String dateEd

    ){

        ResponseModel responseModel = new ResponseModel();
        responseModel.setData(new HashMap<>());
        try {
            List<Order> orderList = orderCustomService.findOrderbyCriteriaRoleFactoryAdmin(facCode,orderNum,customer,status,amountSt,amountEd,dateSt,dateEd);
            String jsonReturn  = (JSONUtil.getJSONSerializer()
                    .include("id")
                    .include("submitDate")
                    .include("orderNumber")
                    .include("total.amount")
                    .include("subTotal.amount")
                    .include("status")
                    .include("status.*")
                    .include("orderStatus")
                    .include("orderStatus.*")
                    .include("orderStatus.friendlyType")
                    .include("orderStatus.type")
                    .include("name")
                    .include("customer.mainEntityName")
                    .include("customer.username")
                    .exclude("*")
                    .serialize(orderList));

            responseModel.setSuccess(true);
            responseModel.setData(JSONUtil.listFromJson(jsonReturn));

            if(orderList!=null && !orderList.isEmpty()) {
                responseModel.setMessage("Order "+orderList.size()+" records");
            }else {
                responseModel.setMessage("No Data Found");
            }

        } catch (Exception e) {
            LOG.error("Exception ");
            responseModel.setSuccess(false);
            responseModel.setMessage(e.getMessage());
            e.printStackTrace();
        }

        return responseModel;
    }

    @GetMapping(value = "/changeStatusOrder/orderNumber/{orderNumber}/status/{status}")
    public ResponseModel changeStatusOrderNumber(@PathVariable(value = "orderNumber")String orderNumber,
                                                 @PathVariable(value = "status")String status){
        LOG.info(" ===- Change status order "+orderNumber+" to "+status+" -===");

        try{
            return  orderCustomService.changeStatusOrder(orderNumber,status);
        }catch (Exception e){
            LOG.error("Exception ");
            e.printStackTrace();
            return null;
        }

    }
}

package com.community.admin.web.repository.impl;

import com.community.admin.web.repository.PoorlyReportRepositoryCustom;
import com.community.admin.web.util.ValidateUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository("blPoorlySoldProductReportCustomRepo")
public class PoorlyReportRepositoryCustomImpl implements PoorlyReportRepositoryCustom {

	private static final Log LOG = LogFactory.getLog(PoorlyReportRepositoryCustomImpl.class);

	@PersistenceContext(unitName = "blPU", type = PersistenceContextType.TRANSACTION)
	protected EntityManager em;


	@Override
	public List<Map<String, Object>> queryReportByMonth_RoleAdmin(String manFac,String facCode, String year, String month, String limitMax) {


		LOG.info("===- queryReportByMonth_RoleAdmin -===");
		LOG.info("===- limitMax : "+limitMax+" -===");
		try{

			List<Object[]> listfromQuery = new ArrayList<Object[]>();
			List<Map<String, Object>> result = new ArrayList<>();

			boolean flag_check_manufac = false;
			boolean flag_check_limit = false;
			boolean flag_check_facCode = false;

			StringBuilder sqlquery = new StringBuilder();

			sqlquery.append("   WITH list_poorly_product AS ( ");
			sqlquery.append("                                SELECT  sku.name as product_name , ");
			sqlquery.append("                                                prd.manufacture as creator, ");
			sqlquery.append("                                                to_char(sku.active_start_date,'YYYY-MM') as date_active, ");
			sqlquery.append("                                                CASE ");
			sqlquery.append("                                                    WHEN (odi.quantity)  is  null THEN 0 ");
			sqlquery.append("                                                    ELSE (odi.quantity) ");
			sqlquery.append("                                                END as sum_quant ");
			sqlquery.append("                                FROM blc_sku sku ");
			sqlquery.append("                                LEFT OUTER JOIN blc_order_item odi on odi.name = sku.name ");
			sqlquery.append("                                RIGHT JOIN blc_product prd on prd.product_id = sku.default_product_id ");
			sqlquery.append("                                LEFT JOIN blc_order od on od.order_id = odi.order_id ");
			sqlquery.append("                             WHERE to_char(sku.active_start_date,'YYYY') = :year ");
			sqlquery.append("                             AND to_char(sku.active_start_date,'MM') = :month ");

			/* condition in list_poorly_product */
			if(!"".equals(facCode) && null != facCode){
				sqlquery.append("  AND od.name = :facCode ");
				flag_check_facCode=true;
			}
			if(!"".equals(manFac) && null != manFac){
			sqlquery.append("  AND prd.manufacture like :manFac ");
				flag_check_manufac=true;
			}

			sqlquery.append("                                ) ");
			sqlquery.append("  					  SELECT product_name as prd_name , ");
			sqlquery.append("                            creator as prd_manufac , ");
			sqlquery.append("                            sum(sum_quant) as sum_amount , ");
			sqlquery.append("                            date_active as prd_date ");
			sqlquery.append("                     FROM list_poorly_product ");
			sqlquery.append("                     GROUP BY list_poorly_product.product_name,list_poorly_product.creator, list_poorly_product.date_active ");
			sqlquery.append("                     ORDER BY sum(list_poorly_product.sum_quant), list_poorly_product.date_active ");

			if(!"ALL".equals(limitMax.toUpperCase())){
				sqlquery.append(" LIMIT :max ");
				flag_check_limit = true;
			}

			Query query = em.createNativeQuery(sqlquery.toString());
			if(flag_check_facCode){
				query.setParameter("facCode", facCode);
			}if(flag_check_manufac){
				query.setParameter("manFac", "%"+manFac+"%");
			}

			query.setParameter("year", year);
			if(flag_check_limit){
				query.setParameter("max",Long.valueOf(limitMax));
			}
			if(month.length() == 1)
				month = "0".concat(month);
			query.setParameter("month",month);
			listfromQuery = query.getResultList();
			if(ValidateUtil.isNotEmpty(listfromQuery)){
				for(Object[] o : listfromQuery){
					Map<String, Object> mapResult = new HashMap();
					mapResult.put("productName", o[0]);
					mapResult.put("manufacture", o[1]);
					mapResult.put("itemAmount", o[2]);

					result.add(mapResult);
				}
			}
			return result;

		}catch (Exception e){
			e.printStackTrace();
			throw new RuntimeException();
		}

	}

	@Override
	public List<Map<String, Object>> queryReportByYear_RoleAdmin(String manFac,String facCode, String year, String limitMax) {
		LOG.info("===- queryReportByYear_RoleAdmin -===");
		LOG.info("===- limitMax : "+limitMax+" -===");

		try{
			List<Object[]> listfromQuery = new ArrayList<Object[]>();
			List<Map<String, Object>> result = new ArrayList<>();

			boolean flag_check_manufac = false;
			boolean flag_check_limit = false;
			boolean flag_check_facCode = false;

			StringBuilder sqlquery = new StringBuilder();


			sqlquery.append("   WITH list_poorly_product AS ( ");
			sqlquery.append("                                SELECT  (sku.name) as product_name , ");
			sqlquery.append("                                                prd.manufacture as creator, ");
			sqlquery.append("                                                to_char(sku.active_start_date,'YYYY') as date_active, ");
			sqlquery.append("                                                CASE ");
			sqlquery.append("                                                    WHEN (odi.quantity)  is  null THEN 0 ");
			sqlquery.append("                                                    ELSE (odi.quantity) ");
			sqlquery.append("                                                END as sum_quant ");
			sqlquery.append("                                FROM blc_sku sku ");
			sqlquery.append("                                LEFT OUTER JOIN blc_order_item odi on odi.name = sku.name ");
			sqlquery.append("                                RIGHT JOIN blc_product prd on prd.product_id = sku.default_product_id ");
			sqlquery.append("                                LEFT JOIN blc_order od on od.order_id = odi.order_id ");
			sqlquery.append("                                WHERE to_char(sku.active_start_date,'YYYY') = :year ");

			/* condition in list_poorly_product */
			if(!"".equals(facCode) && null != facCode){
				sqlquery.append("  AND od.name = :facCode ");
				flag_check_facCode=true;
			}

			if(!"".equals(manFac) && null != manFac){
				sqlquery.append("  AND prd.manufacture like :manFac ");
				flag_check_manufac=true;
			}

			sqlquery.append("                                )  ");
			sqlquery.append("  					  SELECT product_name as prd_name , ");
			sqlquery.append("                            creator as prd_manufac , ");
			sqlquery.append("                            sum(sum_quant) as sum_amount , ");
			sqlquery.append("                            date_active as prd_date ");
			sqlquery.append("                     FROM list_poorly_product ");
			sqlquery.append("                     GROUP BY list_poorly_product.product_name,list_poorly_product.creator, list_poorly_product.date_active ");
			sqlquery.append("                     ORDER BY sum(list_poorly_product.sum_quant), list_poorly_product.date_active ");

			/* limit data*/
			if(!"ALL".equals(limitMax.toUpperCase())){
				sqlquery.append(" LIMIT :max ");
				flag_check_limit = true;
			}

			Query query = em.createNativeQuery(sqlquery.toString());
			if(flag_check_facCode){
				query.setParameter("facCode", facCode);
			}
			if(flag_check_manufac){
				query.setParameter("manFac", "%"+manFac+"%");
			}

			query.setParameter("year", year);
			if(flag_check_limit){
				query.setParameter("max",Long.valueOf(limitMax));
			}
			listfromQuery = query.getResultList();
			if(ValidateUtil.isNotEmpty(listfromQuery)){
				for(Object[] o : listfromQuery){
					Map<String, Object> mapResult = new HashMap();
					mapResult.put("productName", o[0]);
					mapResult.put("manufacture", o[1]);
					mapResult.put("itemAmount", o[2]);
					mapResult.put("year", o[3]);

					result.add(mapResult);
				}
			}
			return result;
		}catch (Exception e){
			e.printStackTrace();
			throw new RuntimeException();
		}

	}

	@Override
	public List<Map<String, Object>> queryReportByMonth_RoleVender(String manFac, String year, String month, String limitMax) {
		LOG.info("===- queryReportByMonth_RoleVender -===");
		LOG.info("===- limitMax : "+limitMax+" -===");
		try{

			List<Object[]> listfromQuery = new ArrayList<Object[]>();
			List<Map<String, Object>> result = new ArrayList<>();

			boolean flag_check_manufac = false;
			boolean flag_check_limit = false;

			StringBuilder sqlquery = new StringBuilder();

			sqlquery.append("   WITH list_poorly_product AS ( ");
			sqlquery.append("                                SELECT  sku.name as product_name , ");
			sqlquery.append("                                                prd.manufacture as creator, ");
			sqlquery.append("                                                to_char(sku.active_start_date,'YYYY-MM') as date_active, ");
			sqlquery.append("                                                CASE ");
			sqlquery.append("                                                    WHEN (odi.quantity)  is  null THEN 0 ");
			sqlquery.append("                                                    ELSE (odi.quantity) ");
			sqlquery.append("                                                END as sum_quant ");
			sqlquery.append("                                FROM blc_sku sku ");
			sqlquery.append("                                LEFT OUTER JOIN blc_order_item odi on odi.name = sku.name ");
			sqlquery.append("                                RIGHT JOIN blc_product prd on prd.product_id = sku.default_product_id ");
			sqlquery.append("                                LEFT JOIN blc_order od on od.order_id = odi.order_id ");
			sqlquery.append("                             WHERE to_char(sku.active_start_date,'YYYY') = :year ");
			sqlquery.append("                             AND to_char(sku.active_start_date,'MM') = :month ");

			/* condition in list_poorly_product */
			if(!"".equals(manFac) && null != manFac){
				sqlquery.append("  AND prd.manufacture like :manFac ");
				flag_check_manufac=true;
			}

			sqlquery.append("                                ) ");
			sqlquery.append("  					  SELECT product_name as prd_name , ");
			sqlquery.append("                            creator as prd_manufac , ");
			sqlquery.append("                            sum(sum_quant) as sum_amount , ");
			sqlquery.append("                            date_active as prd_date ");
			sqlquery.append("                     FROM list_poorly_product ");
			sqlquery.append("                     GROUP BY list_poorly_product.product_name,list_poorly_product.creator, list_poorly_product.date_active ");
			sqlquery.append("                     ORDER BY sum(list_poorly_product.sum_quant), list_poorly_product.date_active ");

			/* limit data*/
			if(!"ALL".equals(limitMax.toUpperCase())){
				sqlquery.append(" LIMIT :max ");
				flag_check_limit = true;
			}

			Query query = em.createNativeQuery(sqlquery.toString());
			if(flag_check_manufac){
				query.setParameter("manFac", "%"+manFac+"%");
			}

			query.setParameter("year", year);
			if(flag_check_limit){
				query.setParameter("max",Long.valueOf(limitMax));
			}
			if(month.length() == 1)
				month = "0".concat(month);
			query.setParameter("month",month);
			listfromQuery = query.getResultList();
			if(ValidateUtil.isNotEmpty(listfromQuery)){
				for(Object[] o : listfromQuery){
					Map<String, Object> mapResult = new HashMap();
					mapResult.put("productName", o[0]);
					mapResult.put("manufacture", o[1]);
					mapResult.put("itemAmount", o[2]);

					result.add(mapResult);
				}
			}
			return result;

		}catch (Exception e){
			e.printStackTrace();
			throw new RuntimeException();
		}
	}

	@Override
	public List<Map<String, Object>> queryReportByYear_RoleVender(String manFac, String year, String limitMax) {
		LOG.info("===- queryReportByYear_RoleVender -===");
		LOG.info("===- limitMax : "+limitMax+" -===");

		try{
			List<Object[]> listfromQuery = new ArrayList<Object[]>();
			List<Map<String, Object>> result = new ArrayList<>();

			boolean flag_check_manufac = false;
			boolean flag_check_limit = false;

			StringBuilder sqlquery = new StringBuilder();


			sqlquery.append("   WITH list_poorly_product AS ( ");
			sqlquery.append("                                SELECT  (sku.name) as product_name , ");
			sqlquery.append("                                                prd.manufacture as creator, ");
			sqlquery.append("                                                to_char(sku.active_start_date,'YYYY') as date_active, ");
			sqlquery.append("                                                CASE ");
			sqlquery.append("                                                    WHEN (odi.quantity)  is  null THEN 0 ");
			sqlquery.append("                                                    ELSE (odi.quantity) ");
			sqlquery.append("                                                END as sum_quant ");
			sqlquery.append("                                FROM blc_sku sku ");
			sqlquery.append("                                LEFT OUTER JOIN blc_order_item odi on odi.name = sku.name ");
			sqlquery.append("                                RIGHT JOIN blc_product prd on prd.product_id = sku.default_product_id ");
			sqlquery.append("                                LEFT JOIN blc_order od on od.order_id = odi.order_id ");
			sqlquery.append("                                WHERE to_char(sku.active_start_date,'YYYY') = :year ");

			/* condition in list_poorly_product */

			if(!"".equals(manFac) && null != manFac){
				sqlquery.append("  AND prd.manufacture like :manFac ");
				flag_check_manufac=true;
			}

			sqlquery.append("                                ) ");
			sqlquery.append("  					  SELECT product_name as prd_name , ");
			sqlquery.append("                            creator as prd_manufac , ");
			sqlquery.append("                            sum(sum_quant) as sum_amount , ");
			sqlquery.append("                            date_active as prd_date ");
			sqlquery.append("                     FROM list_poorly_product ");
			sqlquery.append("                     GROUP BY list_poorly_product.product_name,list_poorly_product.creator, list_poorly_product.date_active ");
			sqlquery.append("                     ORDER BY sum(list_poorly_product.sum_quant), list_poorly_product.date_active ");

			/* limit data*/
			if(!"ALL".equals(limitMax.toUpperCase())){
				sqlquery.append(" LIMIT :max ");
				flag_check_limit = true;
			}

			Query query = em.createNativeQuery(sqlquery.toString());

			if(flag_check_manufac){
				query.setParameter("manFac", "%"+manFac+"%");
			}

			query.setParameter("year", year);
			if(flag_check_limit){
				query.setParameter("max",Long.valueOf(limitMax));
			}
			listfromQuery = query.getResultList();
			if(ValidateUtil.isNotEmpty(listfromQuery)){
				for(Object[] o : listfromQuery){
					Map<String, Object> mapResult = new HashMap();
					mapResult.put("productName", o[0]);
					mapResult.put("manufacture", o[1]);
					mapResult.put("year", o[3]);
					mapResult.put("itemAmount", o[2]);

					result.add(mapResult);
				}
			}
			return result;
		}catch (Exception e){
			e.printStackTrace();
			throw new RuntimeException();
		}
	}
}

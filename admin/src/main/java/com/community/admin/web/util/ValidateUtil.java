package com.community.admin.web.util;

import java.util.List;

public class ValidateUtil {

    public static boolean isNotEmpty(List checkList) {
        Boolean returnValue = false;
        if (checkList != null && !checkList.isEmpty() && checkList.size() > 0) {
            returnValue = true;
        }
        return returnValue;
    }

    public static String isNull(Object o){
        if(o == null){
            return "";
        }else{
          return   String.valueOf(o);
        }
    }

}

package com.community.admin.web.controller;

import org.broadleafcommerce.admin.web.controller.entity.AdminBaseProductController;
import org.broadleafcommerce.admin.web.controller.entity.AdminProductController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller("blAdminVendorProductController")
@RequestMapping("/vendor/" + AdminProductController.SECTION_KEY)
public class AdminVendorProductController extends AdminProductController{


}

package com.community.admin.web.service;

import com.community.admin.web.model.ResponseModel;
import org.broadleafcommerce.core.order.domain.Order;
import java.util.List;

public interface OrderCustomService {

	List<Order> findOrderbyCriteriaRoleFactoryAdmin(
			String facCode,
			String orderNum,
			String customer,
			String status,
			String amountSt,
			String amountEd,
			String dateSt,
			String dateEd
	);

	List<Order> findOrderbyCriteriaRoleAdmin(
			String facCode,
			String orderNum,
			String customer,
			String status,
			String amountSt,
			String amountEd,
			String dateSt,
			String dateEd
	);

	ResponseModel changeStatusOrder(String orderNumber, String status);

}

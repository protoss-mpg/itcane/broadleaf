package com.community.admin.web.repository.impl;

import com.community.admin.web.repository.SummaryReportRepositoryCustom;
import com.community.admin.web.util.ValidateUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository("blSummaryReportCustomRepo")
public class SummaryReportRepositoryCustomImpl implements SummaryReportRepositoryCustom {

	private static final Log LOG = LogFactory.getLog(SummaryReportRepositoryCustomImpl.class);

	@PersistenceContext(unitName = "blPU", type = PersistenceContextType.TRANSACTION)
	protected EntityManager em;


	@Override
	public List<Map<String, Object>> queryReportDataByYear_RoleAdmin(String manufacture, String facCode, String year) {
		LOG.info("===- queryReportDataByYear_RoleAdmin -===");
		try{
			List<Object[]> listfromQuery = new ArrayList<Object[]>();
			List<Map<String, Object>> result = new ArrayList<>();

			boolean flag_check_manufac= false;
			boolean flag_check_facCode= false;

			StringBuilder sqlquery = new StringBuilder();

			/**/
			sqlquery.append(" WITH list_order_summary_admin as ( ");
			sqlquery.append("     SELECT DISTINCT CASE ");
			sqlquery.append("                        WHEN od.name is null ");
			sqlquery.append("                           THEN '' ");
			sqlquery.append("                        ELSE od.name ");
			sqlquery.append("                        END                         as fac_code, ");
			sqlquery.append("                    CASE ");
			sqlquery.append("                        WHEN prd.manufacture is null ");
			sqlquery.append("                            THEN '' ");
			sqlquery.append("                        ELSE prd.manufacture ");
			sqlquery.append("                        END                         as manufac, ");
			sqlquery.append("                    count(od.order_number)          as sum_order, ");
			sqlquery.append("                    sum(odi.quantity)               as sum_quantity, ");
			sqlquery.append("                    sum(od.order_total)             as sum_amount, ");
			sqlquery.append("                    od.submit_date submit_date ");
			sqlquery.append("    FROM blc_discrete_order_item doi ");
			sqlquery.append("             LEFT JOIN blc_order od on doi.order_item_id = od.order_id ");
			sqlquery.append("             LEFT JOIN blc_order_item odi on odi.order_id = od.order_id ");
			sqlquery.append("             LEFT JOIN blc_product prd on prd.product_id = doi.product_id ");
			sqlquery.append("    WHERE od.submit_date is not null ");
            sqlquery.append("    AND od.order_status = 'APPROVE' ");
			sqlquery.append("    AND to_char(od.submit_date, 'YYYY') = :year ");
			if(!"".equals(facCode) && null != facCode){
				sqlquery.append("  AND od.name = :facCode ");
				flag_check_facCode=true;
			}
			if(!"".equals(manufacture) && null != manufacture){
				sqlquery.append("  AND prd.manufacture like :manFac ");
				flag_check_manufac=true;
			}
			sqlquery.append("    GROUP BY od.name,prd.manufacture,od.submit_date ");
			sqlquery.append(" ) ");
			sqlquery.append(" SELECT ");
			sqlquery.append("       fac_code as fac, ");
			sqlquery.append("       manufac as manfac, ");
			sqlquery.append("       sum(sum_order) as sum_od, ");
			sqlquery.append("       sum(sum_quantity) as sum_quant, ");
			sqlquery.append("       sum(sum_amount) as sum_total, ");
			sqlquery.append("       to_char(submit_date,'YYYY')  as sDate ");
			sqlquery.append(" FROM list_order_summary_admin ");
			sqlquery.append(" GROUP BY fac, manufac,sDate ");
			sqlquery.append(" ORDER BY sDate desc, fac asc ");

			/**/


			Query query = em.createNativeQuery(sqlquery.toString());

			if(flag_check_facCode){
				query.setParameter("facCode", facCode);
			}

			if(flag_check_manufac){
				query.setParameter("manFac", "%"+manufacture+"%");
			}

			query.setParameter("year", year);

			listfromQuery = query.getResultList();
			if(ValidateUtil.isNotEmpty(listfromQuery)){
				for(Object[] o : listfromQuery){
					Map<String, Object> mapResult = new HashMap();
					mapResult.put("factory_code", o[0]);
					mapResult.put("manufacture", o[1]);
					mapResult.put("total_order", o[2]);
					mapResult.put("total_item", o[3]);
					mapResult.put("total_amount", o[4]);
					mapResult.put("year", String.valueOf(o[5]).split("-")[0]);

					result.add(mapResult);
				}
			}
			LOG.info("===- size queryReportDataByYear_RoleAdmin : "+result.size()+" -===");
			return result;
		}catch (Exception e){
			e.printStackTrace();
			throw new RuntimeException();
		}

	}

	@Override
	public List<Map<String, Object>> queryReportDataByMonth_RoleAdmin(String manufacture, String facCode, String year) {
		LOG.info("===- queryReportDataByMonth_RoleAdmin -===");
		try{
			List<Object[]> listfromQuery = new ArrayList<Object[]>();
			List<Map<String, Object>> result = new ArrayList<>();

			boolean flag_check_manufac= false;
			boolean flag_check_facCode= false;

			StringBuilder sqlquery = new StringBuilder();

			/**/
			sqlquery.append(" WITH list_order_summary_admin as ( ");
			sqlquery.append("     SELECT DISTINCT CASE ");
			sqlquery.append("                        WHEN od.name is null ");
			sqlquery.append("                           THEN '' ");
			sqlquery.append("                        ELSE od.name ");
			sqlquery.append("                        END                         as fac_code, ");
			sqlquery.append("                    CASE ");
			sqlquery.append("                        WHEN prd.manufacture is null ");
			sqlquery.append("                            THEN '' ");
			sqlquery.append("                        ELSE prd.manufacture ");
			sqlquery.append("                        END                         as manufac, ");
			sqlquery.append("                    count(od.order_number)          as sum_order, ");
			sqlquery.append("                    sum(odi.quantity)               as sum_quantity, ");
			sqlquery.append("                    sum(od.order_total)             as sum_amount, ");
			sqlquery.append("                    od.submit_date submit_date ");
			sqlquery.append("    FROM blc_discrete_order_item doi ");
			sqlquery.append("             LEFT JOIN blc_order od on doi.order_item_id = od.order_id ");
			sqlquery.append("             LEFT JOIN blc_order_item odi on odi.order_id = od.order_id ");
			sqlquery.append("             LEFT JOIN blc_product prd on prd.product_id = doi.product_id ");
			sqlquery.append("    WHERE od.submit_date is not null ");
            sqlquery.append("    AND od.order_status = 'APPROVE' ");
			sqlquery.append("    AND to_char(od.submit_date, 'YYYY') = :year ");
			if(!"".equals(facCode) && null != facCode){
				sqlquery.append("  AND od.name = :facCode ");
				flag_check_facCode=true;
			}
			if(!"".equals(manufacture) && null != manufacture){
				sqlquery.append("  AND prd.manufacture like :manFac ");
				flag_check_manufac=true;
			}
			sqlquery.append("    GROUP BY od.name,prd.manufacture,od.submit_date ");
			sqlquery.append(" ) ");
			sqlquery.append(" SELECT ");
			sqlquery.append("       fac_code as fac, ");
			sqlquery.append("       manufac as manfac, ");
			sqlquery.append("       sum(sum_order) as sum_od, ");
			sqlquery.append("       sum(sum_quantity) as sum_quant, ");
			sqlquery.append("       sum(sum_amount) as sum_total, ");
			sqlquery.append("       to_char(submit_date,'YYYY-MM')  as sDate ");
			sqlquery.append(" FROM list_order_summary_admin ");
			sqlquery.append(" GROUP BY fac, manufac,sDate ");
			sqlquery.append(" ORDER BY sDate desc, fac asc ");

			Query query = em.createNativeQuery(sqlquery.toString());

			if(flag_check_facCode){
				query.setParameter("facCode", facCode);
			}

			if(flag_check_manufac){
				query.setParameter("manFac", "%"+manufacture+"%");
			}

			query.setParameter("year", year);

			listfromQuery = query.getResultList();
			if(ValidateUtil.isNotEmpty(listfromQuery)){
				for(Object[] o : listfromQuery){
					Map<String, Object> mapResult = new HashMap();
					mapResult.put("factory_code", o[0]);
					mapResult.put("manufacture", o[1]);
					mapResult.put("total_order", o[2]);
					mapResult.put("total_item", o[3]);
					mapResult.put("total_amount", o[4]);
					mapResult.put("year", String.valueOf(o[5]).split("-")[0]);
					mapResult.put("month", String.valueOf(o[5]).split("-")[1]);

					result.add(mapResult);
				}
			}
			LOG.info("===- size queryReportDataByMonth_RoleAdmin : "+result.size()+" -===");
			return result;
		}catch (Exception e){
			e.printStackTrace();
			throw new RuntimeException();
		}
	}

	@Override
	public List<Map<String, Object>> queryReportDataByYear_RoleVender(String manufacture, String year) {
		LOG.info("====- queryReportDataByYear_RoleVender -===");
		try{
			List<Object[]> listfromQuery = new ArrayList<Object[]>();
			List<Map<String, Object>> result = new ArrayList<>();

			StringBuilder sqlquery = new StringBuilder();
			/**/
			sqlquery.append(" WITH list_order_summary_vender as ( ");
			sqlquery.append("    SELECT DISTINCT ");
			sqlquery.append("        od.submit_date as sDate, ");
			sqlquery.append("        'ALL' as fac, ");
			sqlquery.append("        CASE ");
			sqlquery.append("            WHEN prd.manufacture is null ");
			sqlquery.append("                THEN '' ");
			sqlquery.append("            ELSE prd.manufacture ");
			sqlquery.append("            END as manufac, ");
			sqlquery.append("        count(od.order_number) as sum_order, ");
			sqlquery.append("        sum(odi.quantity) as sum_quantity, ");
			sqlquery.append("        sum(od.order_total) as sum_amount ");
			sqlquery.append("    FROM blc_discrete_order_item doi ");
			sqlquery.append("             LEFT JOIN     blc_order od on doi.order_item_id = od.order_id ");
			sqlquery.append("             LEFT JOIN  blc_order_item odi on odi.order_id = od.order_id ");
			sqlquery.append("             LEFT JOIN   blc_product prd on prd.product_id = doi.product_id ");
			sqlquery.append("    WHERE od.submit_date is not null ");
			sqlquery.append("      AND od.order_status = 'APPROVE' ");
			sqlquery.append("      AND prd.manufacture = :manufacture ");
			sqlquery.append("      AND to_char(od.submit_date,'YYYY') = :year ");
			sqlquery.append("    GROUP BY fac, prd.manufacture, sDate ");
			sqlquery.append(" ) ");
			sqlquery.append(" SELECT ");
			sqlquery.append(" to_char(sDate,'YYYY') as submit_date, ");
			sqlquery.append(" fac as fac_all, ");
			sqlquery.append(" manufac as manufac, ");
			sqlquery.append(" sum(sum_order) as sum_od, ");
			sqlquery.append(" sum(sum_quantity) as sum_quant, ");
			sqlquery.append(" sum(sum_amount) as sum_all ");
			sqlquery.append(" FROM list_order_summary_vender ");
			sqlquery.append(" GROUP BY submit_date ,fac_all,manufac ");
			sqlquery.append(" ORDER BY submit_date desc, fac_all asc ");
			/**/

			Query query = em.createNativeQuery(sqlquery.toString());

			query.setParameter("year", year);
			query.setParameter("manufacture", manufacture);

			listfromQuery = query.getResultList();
			if(ValidateUtil.isNotEmpty(listfromQuery)){
				for(Object[] o : listfromQuery){
					Map<String, Object> mapResult = new HashMap();
					mapResult.put("year", o[0]);
					mapResult.put("factory", o[1]);
					mapResult.put("total_order", o[3]);
					mapResult.put("total_item", o[4]);
					mapResult.put("total_amount", o[5]);

					result.add(mapResult);
				}
			}
			LOG.info("====- size queryReportDataByYear_RoleVender : "+result.size()+" -===");
			return result;
		}catch (Exception e){
			e.printStackTrace();
			throw new RuntimeException();
		}
	}

	@Override
	public List<Map<String, Object>> queryReportDataByMonth_RoleVende(String manufacture, String year) {
		LOG.info("====- queryReportDataByMonth_RoleVender -===");
		try{
			List<Object[]> listfromQuery = new ArrayList<Object[]>();
			List<Map<String, Object>> result = new ArrayList<>();

			StringBuilder sqlquery = new StringBuilder();
			sqlquery.append(" WITH list_order_summary_vender as ( ");
			sqlquery.append("    SELECT DISTINCT ");
			sqlquery.append("        od.submit_date as sDate, ");
			sqlquery.append("        'ALL' as fac, ");
			sqlquery.append("        CASE ");
			sqlquery.append("            WHEN prd.manufacture is null ");
			sqlquery.append("                THEN '' ");
			sqlquery.append("            ELSE prd.manufacture ");
			sqlquery.append("            END as manufac, ");
			sqlquery.append("        count(od.order_number) as sum_order, ");
			sqlquery.append("        sum(odi.quantity) as sum_quantity, ");
			sqlquery.append("        sum(od.order_total) as sum_amount ");
			sqlquery.append("    FROM blc_discrete_order_item doi ");
			sqlquery.append("             LEFT JOIN     blc_order od on doi.order_item_id = od.order_id ");
			sqlquery.append("             LEFT JOIN  blc_order_item odi on odi.order_id = od.order_id ");
			sqlquery.append("             LEFT JOIN   blc_product prd on prd.product_id = doi.product_id ");
			sqlquery.append("    WHERE od.submit_date is not null ");
			sqlquery.append("      AND prd.manufacture = :manufacture ");
			sqlquery.append("      AND od.order_status = 'APPROVE' ");
			sqlquery.append("      AND to_char(od.submit_date,'YYYY') = :year ");
			sqlquery.append("    GROUP BY fac, prd.manufacture, sDate ");
			sqlquery.append(" ) ");
			sqlquery.append(" SELECT ");
			sqlquery.append(" to_char(sDate,'YYYY-MM') as submit_date, ");
			sqlquery.append(" fac as fac_all, ");
			sqlquery.append(" manufac as manufac, ");
			sqlquery.append(" sum(sum_order) as sum_od, ");
			sqlquery.append(" sum(sum_quantity) as sum_quant, ");
			sqlquery.append(" sum(sum_amount) as sum_all ");
			sqlquery.append(" FROM list_order_summary_vender ");
			sqlquery.append(" GROUP BY submit_date ,fac_all,manufac ");
			sqlquery.append(" ORDER BY submit_date desc, fac_all asc ");

			Query query = em.createNativeQuery(sqlquery.toString());

			query.setParameter("year", year);
			query.setParameter("manufacture", manufacture);

			listfromQuery = query.getResultList();
			if(ValidateUtil.isNotEmpty(listfromQuery)){
				for(Object[] o : listfromQuery){
					Map<String, Object> mapResult = new HashMap();
					mapResult.put("year", String.valueOf(o[0]).split("-")[0]);
					mapResult.put("month", String.valueOf(o[0]).split("-")[1]);
					mapResult.put("factory", o[1]);
					mapResult.put("total_order", o[3]);
					mapResult.put("total_item", o[4]);
					mapResult.put("total_amount", o[5]);

					result.add(mapResult);
				}
			}
			LOG.info("====- size queryReportDataByMonth_RoleVender : "+result.size()+" -===");
			return result;
		}catch (Exception e){
			e.printStackTrace();
			throw new RuntimeException();
		}
	}
}

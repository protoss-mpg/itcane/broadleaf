package com.community.admin.web.controller;

import org.broadleafcommerce.admin.web.controller.entity.AdminCategoryController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller("blAdminVendorCategoryController")
@RequestMapping("/vendor/" + AdminCategoryController.SECTION_KEY)
public class AdminVendorCategoryController  extends AdminCategoryController{

}

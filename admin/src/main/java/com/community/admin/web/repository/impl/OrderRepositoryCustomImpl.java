package com.community.admin.web.repository.impl;

import com.community.admin.web.dao.OrderCustomDao;
import com.community.admin.web.model.ResponseModel;
import com.community.admin.web.repository.OrderRepositoryCustom;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.broadleafcommerce.core.order.domain.Order;
import org.broadleafcommerce.core.order.service.type.OrderStatus;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

@Repository("blOrderCustomRepo")
public class OrderRepositoryCustomImpl implements OrderRepositoryCustom {
	
	private static final Log LOG = LogFactory.getLog(OrderRepositoryCustomImpl.class);
	
	@PersistenceContext(unitName = "blPU", type = PersistenceContextType.TRANSACTION)
	protected EntityManager em;

    @javax.annotation.Resource(name = "blOrderCustomDao")
	OrderCustomDao orderCustomDao;

	@Transactional
	@Override
	public ResponseModel changeStatusOrder(String orderNumber, String status) {
	    ResponseModel responseModel = new ResponseModel();
        Order orderUpdate = orderCustomDao.findOrderByOrderNumber(orderNumber);

        if(orderUpdate != null){
            LOG.info("=======> FOUND Order : "+orderUpdate.getOrderNumber());
            orderUpdate.setStatus(OrderStatus.getInstance(status));
            responseModel.setSuccess(true);
            em.merge(orderUpdate);
            responseModel.setMessage("Update order "+orderNumber+" to status "+status+" success.");
        }

		return responseModel;
	}
}

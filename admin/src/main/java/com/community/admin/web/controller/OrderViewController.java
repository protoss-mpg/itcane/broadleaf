package com.community.admin.web.controller;

import org.broadleafcommerce.core.inventory.service.type.InventoryType;
import org.broadleafcommerce.core.order.service.type.OrderStatus;
import org.broadleafcommerce.openadmin.web.controller.AdminAbstractController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/" + OrderViewController.SECTION_KEY)
public class OrderViewController extends AdminAbstractController {

    protected static final String SECTION_KEY = "requestOrder";

    public static final OrderStatus ORDER_STATUS_WAIT = new OrderStatus("WAIT", "Wait", true);
    public static final OrderStatus ORDER_STATUS_INIT = new OrderStatus("PREPAREREQ", "Prepare Request", true);
    public static final OrderStatus ORDER_STATUS_IN_PROCESS = new OrderStatus("IN_PROCESS", "In Process", true);
    public static final OrderStatus ORDER_STATUS_APPROVE = new OrderStatus("APPROVE", "Approve", true);
    public static final OrderStatus ORDER_STATUS_REJECT = new OrderStatus("REJECT", "Reject", true);
    public static final OrderStatus ORDER_STATUS_SUBMITTED = new OrderStatus("SUBMITTED", "Submitted", true);
    public static final OrderStatus ORDER_STATUS_CANCELLED = new OrderStatus("CANCELLED", "Cancelled", true);
    public static final OrderStatus ORDER_STATUS_ARCHIVED= new OrderStatus("ARCHIVED", "Archived", true);
    public static final OrderStatus ORDER_STATUS_NAMED = new OrderStatus("NAMED", "Named", true);
    public static final OrderStatus ORDER_STATUS_QUOTE = new OrderStatus("QUOTE", "Quote", true);
    public static final OrderStatus ORDER_STATUS_CSR_OWNED = new OrderStatus("CSR_OWNED", "Owned by CSR", true);
    public static final OrderStatus ORDER_STATUS_TESTED = new OrderStatus("TESTED", "Tested", true);
    public static final InventoryType CHECK_QUANTITY = new InventoryType("WAIT_APPROVE", "Wait Approve");

    @RequestMapping(value = "/listAdmin", method = RequestMethod.GET)
    public String listAdmin(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
        model.addAttribute("customView", "views/order/listAdmin");

        setModelAttributes(model, SECTION_KEY);

        /* add order template to body in  emptyContainer*/
        return "modules/emptyContainer";
    }

    @RequestMapping(value = "/listFactoryAdmin", method = RequestMethod.GET)
    public String listFactoryAdmin(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
        model.addAttribute("customView", "views/order/listFactoryAdmin");

        setModelAttributes(model, SECTION_KEY);

        /* add order template to body in  emptyContainer*/
        return "modules/emptyContainer";
    }

}

package com.community.admin.web.dao;

import org.broadleafcommerce.core.order.domain.Order;

import java.util.List;

public interface OrderCustomDao {

	List<Order> findOrderbyCriteriaRoleFactoryAdmin(
			String facCode,
			String orderNum,
			String customer,
			String status,
			String amountSt,
			String amountEd,
			String dateSt,
			String dateEd
	);

	List<Order> findOrderbyCriteriaRoleAdmin(
			String facCode,
			String orderNum,
			String customer,
			String status,
			String amountSt,
			String amountEd,
			String dateSt,
			String dateEd
	);

	Order findOrderByOrderNumber(String orderNumber);
}

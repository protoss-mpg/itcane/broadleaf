package com.community.admin.web.controller.report;

import com.community.admin.web.service.BestSellerReportService;
import com.community.admin.web.util.ValidateUtil;
import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.*;
import org.broadleafcommerce.core.order.service.type.OrderStatus;
import org.broadleafcommerce.openadmin.web.controller.AdminAbstractController;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/" + BestSellerReportController.SECTION_KEY)
public class BestSellerReportController extends AdminAbstractController {
    protected static final String SECTION_KEY = "report/bestSellerReport";

    public static final OrderStatus ORDER_STATUS_WAIT = new OrderStatus("WAIT", "Wait", true);
    public static final OrderStatus ORDER_STATUS_INIT = new OrderStatus("PREPAREREQ", "Prepare Request", true);
    public static final OrderStatus ORDER_STATUS_IN_PROCESS = new OrderStatus("IN_PROCESS", "In Process", true);
    public static final OrderStatus ORDER_STATUS_APPROVE = new OrderStatus("APPROVE", "Approve", true);
    public static final OrderStatus ORDER_STATUS_REJECT = new OrderStatus("REJECT", "Reject", true);
    public static final OrderStatus ORDER_STATUS_SUBMITTED = new OrderStatus("SUBMITTED", "Submitted", true);
    public static final OrderStatus ORDER_STATUS_CANCELLED = new OrderStatus("CANCELLED", "Cancelled", true);
    public static final OrderStatus ORDER_STATUS_ARCHIVED= new OrderStatus("ARCHIVED", "Archived", true);
    public static final OrderStatus ORDER_STATUS_NAMED = new OrderStatus("NAMED", "Named", true);
    public static final OrderStatus ORDER_STATUS_QUOTE = new OrderStatus("QUOTE", "Quote", true);
    public static final OrderStatus ORDER_STATUS_CSR_OWNED = new OrderStatus("CSR_OWNED", "Owned by CSR", true);
    public static final OrderStatus ORDER_STATUS_TESTED = new OrderStatus("TESTED", "Tested", true);


    @Value("${path.template.best.seller.report}")
    private  String pathTemplateExcel = "";
    @Value("${path.template.best.seller.vender.report}")
    private  String pathTemplateVenderExcel = "";

    @javax.annotation.Resource(name = "blBestSellerReportCustomService")
    BestSellerReportService bestSellerReportService;

    @RequestMapping(value = "/listViewAdmin", method = RequestMethod.GET)
    public String listViewAdmin(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
        model.addAttribute("customView", "views/report/bestSellerReport/adminView");

        setModelAttributes(model, SECTION_KEY);

        /* add order template to body in  emptyContainer*/
        return "modules/emptyContainer";
    }

    @RequestMapping(value = "/listViewVendor", method = RequestMethod.GET)
    public String listViewVendor(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
        model.addAttribute("customView", "views/report/bestSellerReport/venderView");

        setModelAttributes(model, SECTION_KEY);

        /* add order template to body in  emptyContainer*/
        return "modules/emptyContainer";
    }

    @RequestMapping(value = "/generateReportAdmin", method = RequestMethod.GET)
    public ResponseEntity<Resource> generateReportAdmin(
            @RequestParam(value = "manFac", required = false)String manFac,
            @RequestParam(value = "facCode", required = false)String facCode,
            @RequestParam(value = "year", required = false)String year,
            @RequestParam(value = "limitMax")String limitMax,
            HttpServletRequest request){
        LOG.info(" === - generateReport orderSummaryReport - ===");
        Workbook wb = new XSSFWorkbook();

        try{
            String filePath = pathTemplateExcel;
            InputStream inputStream = reteriveByteArrayInputStream(new File(filePath));
            wb = new XSSFWorkbook(inputStream);

            /*get listDataByYear by criteria*/
            List<Map<String,Object>> listResultBestSellerItemByYear = bestSellerReportService.queryReportByYear_RoleAdmin(manFac,facCode,year,limitMax);

            if(!listResultBestSellerItemByYear.isEmpty()){
                XSSFDataFormat format = (XSSFDataFormat) wb.createDataFormat();
                XSSFCellStyle currencyStyle = (XSSFCellStyle) wb.createCellStyle();
                currencyStyle.setDataFormat(format.getFormat("_(* #,##0_);_(* (#,##0);_(* \"0\"_);_(@_)"));
                currencyStyle.setAlignment(CellStyle.ALIGN_RIGHT);

                XSSFCellStyle textCenterStyle = (XSSFCellStyle) wb.createCellStyle();
                textCenterStyle.setAlignment(CellStyle.ALIGN_CENTER);

                XSSFSheet sheet = (XSSFSheet) wb.getSheetAt(0);
                int rowStart = 0;

                for(Map map : listResultBestSellerItemByYear){
                    rowStart++;
                    XSSFRow row = sheet.createRow(rowStart);
                    XSSFCell cell = null;

                    cell = row.createCell(0);
                    cell.setCellValue(String.valueOf(map.get("year")));
                    cell.setCellStyle(textCenterStyle);

                    cell = row.createCell(1);
                    cell.setCellValue(ValidateUtil.isNull((map.get("productName"))));

                    cell = row.createCell(2);
                    cell.setCellValue(ValidateUtil.isNull((map.get("manufacture"))));

                    cell = row.createCell(3);
                    cell.setCellValue(new BigDecimal(String.valueOf(map.get("itemAmount"))).intValue());
                    cell.setCellStyle(currencyStyle);
                }
                sheet.autoSizeColumn(0);
                sheet.autoSizeColumn(1);
                sheet.autoSizeColumn(2);
                sheet.autoSizeColumn(3);
            }else{
                LOG.info(" ===- Not found data listResultBestSellerItemByYear -===");
            }
            /* render data by month */
            for(int i=1; i<=12; i ++){
                List<Map<String,Object>> listResultBestSellerItemByMonth = bestSellerReportService.queryReportByMonth_RoleAdmin(manFac,facCode,year,String.valueOf(i),limitMax);

                if(!listResultBestSellerItemByMonth.isEmpty()){
                    XSSFDataFormat format = (XSSFDataFormat) wb.createDataFormat();
                    XSSFCellStyle currencyStyle = (XSSFCellStyle) wb.createCellStyle();
                    currencyStyle.setDataFormat(format.getFormat("_(* #,##0_);_(* (#,##0);_(* \"0\"_);_(@_)"));
                    currencyStyle.setAlignment(CellStyle.ALIGN_RIGHT);

                    XSSFCellStyle textCenterStyle = (XSSFCellStyle) wb.createCellStyle();
                    textCenterStyle.setAlignment(CellStyle.ALIGN_CENTER);

                    XSSFSheet sheet = (XSSFSheet) wb.getSheetAt(i);
                    int rowStart = 0;

                    for(Map map : listResultBestSellerItemByMonth){
                        rowStart++;
                        XSSFRow row = sheet.createRow(rowStart);
                        XSSFCell cell = null;

                        cell = row.createCell(0);
                        cell.setCellValue(ValidateUtil.isNull((map.get("productName"))));

                        cell = row.createCell(1);
                        cell.setCellValue(ValidateUtil.isNull((map.get("manufacture"))));

                        cell = row.createCell(2);
                        cell.setCellValue(new BigDecimal(String.valueOf(map.get("itemAmount"))).intValue());
                        cell.setCellStyle(currencyStyle);

                    }
                    sheet.autoSizeColumn(0);
                    sheet.autoSizeColumn(1);
                    sheet.autoSizeColumn(2);
                }else{
                    LOG.info(" ===- Not found data listResultBestSellerItemByMonth Month : "+i+" -===");
                }

            }

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            wb.write(outputStream);
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            String current_date = dateFormat.format(new Date());

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(new MediaType("application", "vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
            headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+"BestSellerReport_"+current_date+".xlsx"+"\"");//
            headers.setContentLength(outputStream.size());

            ByteArrayInputStream inputStremResult = new ByteArrayInputStream(outputStream.toByteArray());
            return ResponseEntity
                    .ok()
                    .headers(headers)
                    .body(new InputStreamResource(inputStremResult));

        }catch (IOException ie){
            LOG.error("IOException  msg : "+ie.getMessage());
            ie.printStackTrace();
            throw new RuntimeException();
        }catch (Exception e){
            LOG.error(("Exception msg : "+e.getMessage()));
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    @RequestMapping(value = "/generateReportVender", method = RequestMethod.GET)
    public ResponseEntity<Resource> generateReportVender(
            @RequestParam(value = "manFac", required = false)String manFac,
            @RequestParam(value = "year", required = false)String year,
            @RequestParam(value = "limitMax")String limitMax,

            HttpServletRequest request){
        LOG.info(" === - generateReportVender - ===");
        Workbook wb = new XSSFWorkbook();

        try{
            String filePath = pathTemplateVenderExcel;
            InputStream inputStream = reteriveByteArrayInputStream(new File(filePath));
            wb = new XSSFWorkbook(inputStream);

            /*get listDataByYear by criteria*/
            List<Map<String,Object>> listResultBestSellerItemByYear = bestSellerReportService.queryReportByYear_RoleVender(manFac,year,limitMax);

            if(!listResultBestSellerItemByYear.isEmpty()){
                XSSFDataFormat format = (XSSFDataFormat) wb.createDataFormat();
                XSSFCellStyle currencyStyle = (XSSFCellStyle) wb.createCellStyle();
                currencyStyle.setDataFormat(format.getFormat("_(* #,##0_);_(* (#,##0);_(* \"0\"_);_(@_)"));
                currencyStyle.setAlignment(CellStyle.ALIGN_RIGHT);

                XSSFCellStyle textCenterStyle = (XSSFCellStyle) wb.createCellStyle();
                textCenterStyle.setAlignment(CellStyle.ALIGN_CENTER);

                XSSFSheet sheet = (XSSFSheet) wb.getSheetAt(0);
                int rowStart = 0;

                for(Map map : listResultBestSellerItemByYear){
                    rowStart++;
                    XSSFRow row = sheet.createRow(rowStart);
                    XSSFCell cell = null;

                    cell = row.createCell(0);
                    cell.setCellValue(String.valueOf(map.get("year")));
                    cell.setCellStyle(textCenterStyle);

                    cell = row.createCell(1);
                    cell.setCellValue(ValidateUtil.isNull((map.get("productName"))));

                    cell = row.createCell(2);
                    cell.setCellValue(new BigDecimal(String.valueOf(map.get("itemAmount"))).intValue());
                    cell.setCellStyle(currencyStyle);
                }
                sheet.autoSizeColumn(0);
                sheet.autoSizeColumn(1);
                sheet.autoSizeColumn(2);
            }else{
                LOG.info(" ===- Not found data listResultBestSellerItemByYear -===");
            }
            /* render data by month */
            for(int i=1; i<=12; i ++){
                List<Map<String,Object>> listResultBestSellerItemByMonth = bestSellerReportService.queryReportByMonth_RoleVender(manFac,year,String.valueOf(i),limitMax);

                if(!listResultBestSellerItemByMonth.isEmpty()){
                    XSSFDataFormat format = (XSSFDataFormat) wb.createDataFormat();
                    XSSFCellStyle currencyStyle = (XSSFCellStyle) wb.createCellStyle();
                    currencyStyle.setDataFormat(format.getFormat("_(* #,##0_);_(* (#,##0);_(* \"0\"_);_(@_)"));
                    currencyStyle.setAlignment(CellStyle.ALIGN_RIGHT);

                    XSSFCellStyle textCenterStyle = (XSSFCellStyle) wb.createCellStyle();
                    textCenterStyle.setAlignment(CellStyle.ALIGN_CENTER);

                    XSSFSheet sheet = (XSSFSheet) wb.getSheetAt(i);
                    int rowStart = 0;

                    for(Map map : listResultBestSellerItemByMonth){
                        rowStart++;
                        XSSFRow row = sheet.createRow(rowStart);
                        XSSFCell cell = null;

                        cell = row.createCell(0);
                        cell.setCellValue(ValidateUtil.isNull((map.get("productName"))));

                        cell = row.createCell(1);
                        cell.setCellValue(new BigDecimal(String.valueOf(map.get("itemAmount"))).intValue());
                        cell.setCellStyle(currencyStyle);

                    }
                    sheet.autoSizeColumn(0);
                    sheet.autoSizeColumn(1);
                }else{
                    LOG.info(" ===- Not found data listResultBestSellerItemByMonth Month : "+i+" -===");
                }

            }

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            wb.write(outputStream);
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            String current_date = dateFormat.format(new Date());

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(new MediaType("application", "vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
            headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+"BestSellerReport_"+current_date+".xlsx"+"\"");//
            headers.setContentLength(outputStream.size());

            ByteArrayInputStream inputStremResult = new ByteArrayInputStream(outputStream.toByteArray());
            return ResponseEntity
                    .ok()
                    .headers(headers)
                    .body(new InputStreamResource(inputStremResult));

        }catch (IOException ie){
            LOG.error("IOException  msg : "+ie.getMessage());
            ie.printStackTrace();
            throw new RuntimeException();
        }catch (Exception e){
            LOG.error(("Exception msg : "+e.getMessage()));
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    private ByteArrayInputStream reteriveByteArrayInputStream(File file) throws IOException {
        return new ByteArrayInputStream(FileUtils.readFileToByteArray(file));
    }
}

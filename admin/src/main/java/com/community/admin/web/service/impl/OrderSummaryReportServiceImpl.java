package com.community.admin.web.service.impl;

import com.community.admin.web.repository.SummaryReportRepositoryCustom;
import com.community.admin.web.service.OrderSummaryReportService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service("blOrderSummaryReportCustomService")
@ManagedResource(objectName="org.broadleafcommerce:name=OrderCustomService", description="Order Custom Service", currencyTimeLimit=15)
public class OrderSummaryReportServiceImpl implements OrderSummaryReportService {

	private static final Log LOG = LogFactory.getLog(OrderSummaryReportServiceImpl.class);

	@Resource(name = "blSummaryReportCustomRepo")
	protected SummaryReportRepositoryCustom summaryReportRepositoryCustom;

	@Override
	public List<Map<String, Object>> queryReportDataByYear_RoleAdmin(String manufacture, String facCode, String year) {
		return summaryReportRepositoryCustom.queryReportDataByYear_RoleAdmin(manufacture,facCode,year);
	}

	@Override
	public List<Map<String, Object>> queryReportDataByMonth_RoleAdmin(String manufacture, String facCode, String year) {
		return summaryReportRepositoryCustom.queryReportDataByMonth_RoleAdmin(manufacture,facCode,year);
	}

	@Override
	public List<Map<String, Object>> queryReportDataByYear_RoleVender(String manufacture, String year) {
		return summaryReportRepositoryCustom.queryReportDataByYear_RoleVender(manufacture,year);
	}

	@Override
	public List<Map<String, Object>> queryReportDataByMonth_RoleVende(String manufacture, String year) {
		return summaryReportRepositoryCustom.queryReportDataByMonth_RoleVende(manufacture,year);
	}
}

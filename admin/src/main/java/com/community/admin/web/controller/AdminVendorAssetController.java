package com.community.admin.web.controller;

import org.broadleafcommerce.cms.admin.web.controller.AdminAssetController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller("blAdminVendorAssetController")
@RequestMapping(value="/vendor/" + AdminAssetController.SECTION_KEY)
public class AdminVendorAssetController extends AdminAssetController{
	
	

}

package com.community.admin.web.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;

@RestController
public class ClearTempFileController {
    private static final Log LOG = LogFactory.getLog(ClearTempFileController.class);

    @Value("${file.service.temp.file.base.directory}")
    String pathTempAsset = "";
    @Value("${asset.server.file.system.path}")
    String pathAsset = "";
    @Value("${enable.aop.clear.cache}")
    boolean aopEnable;

    @RequestMapping(value = "/clearTmp", method = RequestMethod.GET)
    public String clearCache() {
        File directory = new File(pathTempAsset);
        if (! directory.exists()){
            LOG.info("Create dir : "+ pathTempAsset);
            directory.mkdir();
        }


        LOG.info("Path Asset = "+ pathTempAsset);
        if(aopEnable){
            File folder = new File(pathTempAsset);
            File[] listOfFiles = folder.listFiles();
            LOG.info("========= CLEAR CACHE ================");
            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isFile()) {
                    listOfFiles[i].delete();
                    LOG.info("File " + listOfFiles[i].getName());
                }
            }
            LOG.info("==================================");
            return "Success";
        }else{
            return "Fail aop has mode disable";
        }



    }
}
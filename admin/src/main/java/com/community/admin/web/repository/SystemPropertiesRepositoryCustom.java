package com.community.admin.web.repository;

import com.community.admin.web.model.ResponseModel;

public interface SystemPropertiesRepositoryCustom {

    ResponseModel findByFriendlyGroup(String friendlyGroup);
}

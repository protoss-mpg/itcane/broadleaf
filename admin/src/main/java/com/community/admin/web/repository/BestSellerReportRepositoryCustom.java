package com.community.admin.web.repository;

import java.util.List;
import java.util.Map;

public interface BestSellerReportRepositoryCustom {

    List<Map<String,Object>> queryReportByMonth_RoleAdmin(String manFac,String facCode, String year, String month, String limitMax);
    List<Map<String,Object>> queryReportByYear_RoleAdmin(String manFac,String facCode, String year, String limitMax);

    List<Map<String,Object>> queryReportByMonth_RoleVender(String manFac, String year, String month, String limitMax);
    List<Map<String,Object>> queryReportByYear_RoleVender(String manFac, String year, String limitMax);

}

package com.community.admin.web.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.community.admin.web.service.UserProfileService;
import com.protosstechnology.commons.util.JSONUtil;

@Service("userProfileService")
public class UserProfileServiceImpl implements UserProfileService{
	
	@Value("${blc.server.ad.user.profile}")
	protected String userProfileServer ;
	
	@Autowired
	RestTemplate restTemplate;

	@Override
	public String getFactoryCode(String username) {
		String url = userProfileServer+username;
		ResponseEntity<String>  jsonData = getJsonData(url);
		String factoryCode = JSONUtil.fromJsonPath(jsonData.getBody(), String.class, "facCode");
		return factoryCode;
	}
	
	public ResponseEntity<String> getJsonData(String urlParam) {
    	return restTemplate.getForEntity(urlParam, String.class);
    }


}

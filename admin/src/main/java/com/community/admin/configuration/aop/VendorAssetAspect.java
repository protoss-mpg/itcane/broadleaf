package com.community.admin.configuration.aop;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.broadleafcommerce.cms.file.domain.StaticAsset;
import org.broadleafcommerce.openadmin.web.form.entity.EntityForm;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;


@Component
@Aspect
public class VendorAssetAspect {
    private static final Log LOG = LogFactory.getLog(VendorAssetAspect.class);


    @Pointcut("execution(* org.broadleafcommerce.cms.admin.web.controller.AdminAssetController.*(..))")
    public void redirectVendor() {
    }

    @Before("redirectVendor()")
    public void redirectVendor(JoinPoint jp) throws Throwable {
        String methodName = jp.getSignature().getName();
        //LOG.info("===================VendorAssetAspect ="+methodName);
        
        
        if(jp.getArgs().length > 1 &&
        		jp.getArgs()[0] instanceof HttpServletRequest &&
        		jp.getArgs()[1] instanceof HttpServletResponse ) {
        	
        	HttpServletRequest httpServletRequest   = (HttpServletRequest) jp.getArgs()[0];
        	HttpServletResponse httpServletResponse = (HttpServletResponse) jp.getArgs()[1];
        	

        	Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        	Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>) authentication.getAuthorities();
        	List<GrantedAuthority> listAuthorities   = new ArrayList<GrantedAuthority>();
        	listAuthorities.addAll(authorities);
        	
        	boolean isVendor = false;
        	for(GrantedAuthority authority :listAuthorities) {
        		if("ROLE_VENDOR".equals(authority.getAuthority())) {
        			isVendor = true;
        			break;
        		}
        	}
        	String queryString = "";
        	if(httpServletRequest.getQueryString()!=null) {
        		queryString = "?"+httpServletRequest.getQueryString();
        	}
        	String newUrl = httpServletRequest.getContextPath()+"/vendor"+httpServletRequest.getServletPath()+queryString;
        	
        	if(isVendor && 
        			httpServletRequest.getServletPath().startsWith("/assets") &&
        			!httpServletRequest.getServletPath().startsWith("/vendor") &&
        			!httpServletRequest.getServletPath().startsWith("/org.broadleafcommerce.cms.file.domain.ImageStaticAssetImpl")) {
        		httpServletResponse.sendRedirect(newUrl);
        	}
        	

            if(isVendor && "viewEntityList".equals(methodName)) {
            	if(jp.getArgs().length > 4 &&
                		jp.getArgs()[4] instanceof MultiValueMap ) {
            		MultiValueMap<String, String> requestParams = (MultiValueMap<String, String>) jp.getArgs()[4];
            		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
            		requestParams.add("title", userName);
            	}
            }
        	
        }
    }
    
    @Before("execution(* org.broadleafcommerce.cms.file.dao.StaticAssetDaoImpl.addOrUpdateStaticAsset(..))")
	public void addOrUpdateStaticAsset(JoinPoint joinPoint){
    	Object[] args = joinPoint.getArgs();
    	StaticAsset asset  = (StaticAsset) args[0];
    	String userName = SecurityContextHolder.getContext().getAuthentication().getName();
    	asset.setTitle(userName);
	}
    

}


package com.community.admin.configuration.aop;


import com.community.admin.web.controller.ClearTempFileController;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
@Aspect
public class UploadFileAspect {
    private static final Log LOG = LogFactory.getLog(UploadFileAspect.class);

    @Autowired
    ClearTempFileController clearTempFileController;


    @Pointcut("execution(* org.broadleafcommerce.cms.admin.web.controller.AdminAssetUploadController.*upload*(..))")
    public void clearCacheTmpFile() {

    }

    @Before("clearCacheTmpFile()")
    public void profile(JoinPoint jp) throws Throwable {
        String methodName = jp.getSignature().getName();
        LOG.info(">>>>>>>>>>>>>> Executing method: " + methodName);
        clearTempFileController.clearCache();
    }

}


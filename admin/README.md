# Build Admin
docker build -t mpg-mmarketplace-admin .

# Run Admin
docker run -v /Users/dev2/Desktop/BL/AssetFile:/usr/local/mmarket/AssetFile -v /Users/dev2/Desktop/BL/TmpAssetFile:/usr/local/mmarket/TmpAssetFile -v /Users/dev2/Desktop/BL/TemplateReport:/usr/local/mmarket/TemplateReport -p 8081:8081 -p 8444:8444 -p 8983:8983 -it mpg-mmarketplace-admin
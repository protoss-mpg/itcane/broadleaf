let toggleClick =2
$( document ).ready(function() {
    if(screen.width > 400){
        $('.toggleCustom').addClass('hide')
        $('.subMenuCustom').addClass('hide')
    }else{
        $('.toggleCustom').removeClass('hide')
    }
	// if ($('.navbar-toggle').is(":visible")){
	// 	$('.main-menu').find('.dropdown').find('ul').removeClass('dropdown-menu');
	// }else{
	// 	$('.main-menu').find('.dropdown').find('ul').addClass('dropdown-menu');
	// }
	//
    convertCurrencyUnit();
    changeCurrencyinCart();
});

$(window).resize(function(){
    if(screen.width > 400){
        $('.toggleCustom').addClass('hide')
    }else{
        $('.toggleCustom').removeClass('hide')
    }
});

function toggleMenu(){
    if(screen.width < 400){
        if(toggleClick % 2 == 0){
            $('.main-menu').find('.dropdown').find('ul').removeClass('dropdown-menu');
            $('.iconCustom').removeClass('fa fa-sort-desc')
            $('.iconCustom').addClass('fa fa-sort-asc')
            toggleClick++;
        }else{
            $('.main-menu').find('.dropdown').find('ul').addClass('dropdown-menu');
            $('.iconCustom').removeClass('fa fa-sort-asc')
            $('.iconCustom').addClass('fa fa-sort-desc')
            toggleClick++;
        }

    }
}


function convertCurrencyUnit() {
    $('.price, .main-price, .mini-cart-subtotal, .cart-estimated-total , .js-orderHistoryRow ').find('span:contains("฿"), span:contains("THB") ,div:contains("THB"), div:contains("฿") ,td:contains("฿") ').text(function () {

        // console.log($(this).text())
        let textReplace = $(this).text()
        if($(this).text().includes('฿')){
            // console.log('case ฿')
            textReplace  = $(this).text().replace('฿','')
            textReplace = textReplace+' บาท'
        }else if($(this).text().includes('THB')){
            // console.log('case THB')
            textReplace  = $(this).text().replace('THB','')
            textReplace = textReplace+' Baht'
        }


        return textReplace;

    });
    
    $('.price').text(function () {

        // console.log($(this).text())
        let textReplace = $(this).text()
        if($(this).text().includes('฿')){
            // console.log('case ฿')
            textReplace  = $(this).text().replace('฿','')
            textReplace = textReplace+' บาท'
        }else if($(this).text().includes('THB')){
            // console.log('case THB')
            textReplace  = $(this).text().replace('THB','')
            textReplace = textReplace+' Baht'
        }


        return textReplace;

    });
    
}


function changeCurrencyinCart() {

    $('.mini-cart-item-price, .unitCurrency').text(function () {

        // console.log($(this).text())
        let textReplace = $(this).text()
        if($(this).text().includes('฿')){
            // console.log('case ฿')
            textReplace  = $(this).text().replace('฿','')
            textReplace = textReplace+' บาท'
        }else if($(this).text().includes('THB')){
            // console.log('case THB')
            textReplace  = $(this).text().replace('THB','')
            textReplace = textReplace+' Baht'
        }


        return textReplace;

    });

}



function convertCurrencyAll() {
    convertCurrencyUnit();
    changeCurrencyinCart();

}
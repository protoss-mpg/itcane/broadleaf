package com.community.controller.systemproperties;

import com.community.model.ResponseModel;
import com.community.service.OrderCustomService;
import com.community.service.SystemPropertiesCustomService;
import com.protosstechnology.commons.util.JSONUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.broadleafcommerce.core.order.domain.Order;
import org.broadleafcommerce.core.order.service.type.OrderStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class SystemPropertiesController {
	
	private static final Log LOG = LogFactory.getLog(SystemPropertiesController.class);

	@Resource(name = "blSystemPropertiesCustomService")
	SystemPropertiesCustomService systemPropertiesCustomService;
	
	@GetMapping(value="/requestSystemProperties/findByFriendlyGroup",produces = "application/json; charset=utf-8")
	public ResponseModel findByFriendlyGroup(@RequestParam("friendlyGroup") String friendlyGroup){

		ResponseModel responseModel = null;
		try {
			 responseModel = systemPropertiesCustomService.findByFriendlyGroup(friendlyGroup);		} catch (Exception e) {
			responseModel.setSuccess(false);
			responseModel.setMessage(e.getMessage());
		}
		
		return responseModel;
	}
}

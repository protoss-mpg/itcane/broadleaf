package com.community.controller.account.custom;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.broadleafcommerce.common.exception.ServiceException;
import org.broadleafcommerce.common.web.controller.BroadleafAbstractController;
import org.broadleafcommerce.core.order.domain.NullOrderImpl;
import org.broadleafcommerce.core.order.domain.Order;
import org.broadleafcommerce.core.order.service.OrderService;
import org.broadleafcommerce.core.pricing.service.exception.PricingException;
import org.broadleafcommerce.core.web.order.CartState;
import org.broadleafcommerce.profile.core.domain.Customer;
import org.broadleafcommerce.profile.core.service.CustomerService;
import com.community.controller.account.custom.RegisterCustomerValidator;
import org.broadleafcommerce.profile.web.core.form.RegisterCustomerForm;
import org.broadleafcommerce.profile.web.core.service.login.LoginService;
import org.broadleafcommerce.profile.web.core.service.register.RegistrationService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

public class BroadleafCustomRegisterController extends BroadleafAbstractController {

	@Value("${use.email.for.site.login:true}")
    protected boolean useEmailForLogin;

    protected static String registerSuccessView = "ajaxredirect:";
    protected static String registerView = "authentication/register";
    
    @Resource(name="blRegistrationService")
    protected RegistrationService registrationService;

    @Resource(name="blCustomerService")
    protected CustomerService customerService;

    @Resource(name="blRegisterMpgCustomerValidator")
    protected RegisterCustomerValidator registerCustomerValidator;

    @Resource(name="blLoginService")
    protected LoginService loginService;

    @Resource(name = "blOrderService")
    protected OrderService orderService;
    
    public String register(RegisterCustomerForm registerCustomerForm, HttpServletRequest request, 
            HttpServletResponse response, Model model) {
        registrationService.addRedirectUrlToForm(registerCustomerForm);

        return getRegisterView();
    }

    public String processRegister(RegisterCustomerForm registerCustomerForm, BindingResult errors, 
            HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServiceException, PricingException {
        
        if (useEmailForLogin) {
            Customer customer = registerCustomerForm.getCustomer();
            customer.setUsername(customer.getEmailAddress());
        }
        
        registerCustomerValidator.validate(registerCustomerForm, errors, useEmailForLogin);
        if (!errors.hasErrors()) {
            Customer newCustomer = customerService.registerCustomer(registerCustomerForm.getCustomer(), 
                    registerCustomerForm.getPassword(), registerCustomerForm.getPasswordConfirm());
            assert(newCustomer != null);
            
            // The next line needs to use the customer from the input form and not the customer returned after registration
            // so that we still have the unencoded password for use by the authentication mechanism.
            loginService.loginCustomer(registerCustomerForm.getCustomer());

            // Need to ensure that the Cart on CartState is owned by the newly registered customer.
            Order cart = CartState.getCart();
            if (cart != null && !(cart instanceof NullOrderImpl) && cart.getEmailAddress() == null) {
                cart.setEmailAddress(newCustomer.getEmailAddress());
                orderService.save(cart, false);
            }
            
            String redirectUrl = registerCustomerForm.getRedirectUrl();
            if (StringUtils.isNotBlank(redirectUrl) && redirectUrl.contains(":")) {
                redirectUrl = null;
            }
            
            if (StringUtils.isNotBlank(redirectUrl) && request.getContextPath().equals(redirectUrl)) {
                redirectUrl = null;
            }
            return StringUtils.isBlank(redirectUrl) ? getRegisterSuccessView() : "redirect:" + redirectUrl;
        } else {
            return getRegisterView();
        }
    }
    
    public RegisterCustomerForm initCustomerRegistrationForm() {
        return registrationService.initCustomerRegistrationForm();
    }

    public boolean isUseEmailForLogin() {
        return useEmailForLogin;
    }

    public void setUseEmailForLogin(boolean useEmailForLogin) {
        this.useEmailForLogin = useEmailForLogin;
    }

    /**
     * Returns the view that will be returned from this controller when the 
     * registration is successful.   The success view should be a redirect (e.g. start with "redirect:" since 
     * this will cause the entire SpringSecurity pipeline to be fulfilled.
     * 
     * By default, returns "redirect:/"
     * 
     * @return the register success view
     */
    public String getRegisterSuccessView() {
        return registerSuccessView;
    }

    /**
     * Returns the view that will be used to display the registration page.
     * 
     * By default, returns "/register"
     * 
     * @return the register view
     */
    public String getRegisterView() {
        return registerView;
    }
}

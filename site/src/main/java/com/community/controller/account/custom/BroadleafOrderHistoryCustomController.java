package com.community.controller.account.custom;

import org.broadleafcommerce.core.order.domain.Order;
import org.broadleafcommerce.core.order.service.OrderService;
import org.broadleafcommerce.core.order.service.type.OrderStatus;
import org.broadleafcommerce.core.web.controller.account.AbstractAccountController;
import org.broadleafcommerce.core.web.service.OrderHistoryService;
import org.broadleafcommerce.profile.web.core.CustomerState;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ui.Model;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

public class BroadleafOrderHistoryCustomController extends AbstractAccountController {

	@Value("${validate.customer.owned.data:true}")
    protected boolean validateCustomerOwnedData;

    protected static String orderHistoryView = "account/orderHistory";
    protected static String orderDetailsView = "account/partials/orderDetails";
    protected static String orderDetailsRedirectView = "account/partials/orderDetails";
    

    public static final OrderStatus ORDER_STATUS_INIT = new OrderStatus("PREPAREREQ", "Prepare Request", true);
    public static final OrderStatus ORDER_STATUS_WAIT = new OrderStatus("WAIT", "Wait", true);
	public static final OrderStatus ORDER_STATUS_REJECT = new OrderStatus("REJECT", "Reject", true);
	public static final OrderStatus ORDER_STATUS_APPROVE = new OrderStatus("APPROVE", "Approve", true);

    @Resource(name = "blOrderHistoryService")
    protected OrderHistoryService orderHistoryService;

    @Resource(name = "blOrderService")
    protected OrderService orderService;

    public String viewOrderHistory(HttpServletRequest request, Model model) {
        List<Order> orders = new ArrayList();
        orders.addAll(orderService.findOrdersForCustomer(CustomerState.getCustomer(),OrderStatus.SUBMITTED));
        orders.addAll(orderService.findOrdersForCustomer(CustomerState.getCustomer(),ORDER_STATUS_WAIT));
        orders.addAll(orderService.findOrdersForCustomer(CustomerState.getCustomer(),ORDER_STATUS_REJECT));
        orders.addAll(orderService.findOrdersForCustomer(CustomerState.getCustomer(),ORDER_STATUS_APPROVE));
        orders.addAll(orderService.findOrdersForCustomer(CustomerState.getCustomer(),ORDER_STATUS_INIT));
        
        orders.sort((s1, s2) -> s2.getSubmitDate().compareTo(s1.getSubmitDate())); 

        model.addAttribute("orders", orders);
        return getOrderHistoryView();
    }

    public String viewOrderDetails(HttpServletRequest request, Model model, String orderNumber) {
        Order order = orderHistoryService.getOrderDetails(orderNumber);

        model.addAttribute("order", order);
        return isAjaxRequest(request) ? getOrderDetailsView() : getOrderDetailsRedirectView();
    }

    public String getOrderHistoryView() {
        return orderHistoryView;
    }

    public String getOrderDetailsView() {
        return orderDetailsView;
    }

    public String getOrderDetailsRedirectView() {
        return orderDetailsRedirectView;
    }

    protected void validateCustomerOwnedData(Order order) {
        orderHistoryService.validateCustomerOwnedData(order);
    }

}

package com.community.controller.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.broadleafcommerce.core.order.domain.Order;
import org.broadleafcommerce.core.order.service.type.OrderStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.community.model.ResponseModel;
import com.community.service.OrderCustomService;
import com.protosstechnology.commons.util.JSONUtil;

@RestController
public class OrderController {
	
	private static final Log LOG = LogFactory.getLog(OrderController.class);

	@Resource(name = "blOrderCustomService")
	OrderCustomService orderCustomService;
	
	@GetMapping(value="/orderwait/{factoryCode}",produces = "application/json; charset=utf-8")
	public ResponseModel getWaitOrderByFactoryCode(@PathVariable("factoryCode") String factoryCode){
		LOG.debug("======================== getWaitOrderByFactoryCode factoryCode="+factoryCode);
		
		ResponseModel responseModel = new ResponseModel();
		try {
			List<Order> orderList = orderCustomService.findOrdersForFactoryCode(factoryCode, OrderStatus.getInstance("WAIT"));
			String jsonReturn  = (JSONUtil.getJSONSerializer()
					.include("submitDate")
					.include("orderNumber")
					.include("total.amount")
					.include("subTotal.amount")
					.include("status.friendlyType")
					.include("name")
					.include("customer.mainEntityName")
					.include("customer.username")
					.exclude("*")
					.serialize(orderList));
			
			responseModel.setSuccess(true);
			responseModel.setData(JSONUtil.listFromJson(jsonReturn));
			
			if(orderList!=null && !orderList.isEmpty()) {
				responseModel.setMessage("Order "+orderList.size()+" records");
			}else {
				responseModel.setMessage("No Data Found");
			}
		} catch (Exception e) {
			responseModel.setSuccess(false);
			responseModel.setMessage(e.getMessage());
		}
		
		return responseModel;
	}
}

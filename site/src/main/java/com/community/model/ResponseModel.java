package com.community.model;

import java.util.HashMap;

public class ResponseModel {

	private Boolean success;
	private String message;
	private Object data;
	
	public ResponseModel() {
		setSuccess(false);
        setMessage("Process Fail");
        setData(new HashMap());
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}


}

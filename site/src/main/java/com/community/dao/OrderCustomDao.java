package com.community.dao;

import java.util.List;

import org.broadleafcommerce.core.order.domain.Order;
import org.broadleafcommerce.core.order.service.type.OrderStatus;

public interface OrderCustomDao {

	List<Order> readOrdersForCustomerName(String name, OrderStatus orderStatus);
}

package com.community.repository.impl;

import com.community.model.ResponseModel;
import com.community.repository.SystemPropertiesRepositoryCustom;
import com.community.util.ValidateUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository("blSystemPropertiesCustomRepo")
public class SystemPropertiesRepositoryCustomImpl implements SystemPropertiesRepositoryCustom {
	
	private static final Log LOG = LogFactory.getLog(SystemPropertiesRepositoryCustomImpl.class);
	
	@PersistenceContext(unitName = "blPU", type = PersistenceContextType.TRANSACTION)
	protected EntityManager em;

	@Override
	public ResponseModel findByFriendlyGroup(String friendlyGroup) {
		ResponseModel responseModel = new ResponseModel();
		try{
			List<Object[]> listfromQuery = new ArrayList<Object[]>();
			List<Map<String, Object>> result = new ArrayList<>();

			StringBuilder sqlquery = new StringBuilder();
			/*section for append query*/
			sqlquery.append("    SELECT ");
			sqlquery.append("          blc_system_property_id, ");
			sqlquery.append("          friendly_group, ");
			sqlquery.append("          friendly_name, ");
			sqlquery.append("          friendly_tab, ");
			sqlquery.append("          property_name, ");
			sqlquery.append("          override_generated_prop_name, ");
			sqlquery.append("          property_type, ");
			sqlquery.append("          property_value ");
			sqlquery.append("   FROM blc_system_property ");
			sqlquery.append("   WHERE friendly_group = :friendlyGroup ");
			sqlquery.append("   ORDER BY property_name asc ");

			Query query = em.createNativeQuery(sqlquery.toString());
			/*section for set parameter*/
			query.setParameter("friendlyGroup", friendlyGroup);

			listfromQuery = query.getResultList();
			if(ValidateUtil.isNotEmpty(listfromQuery)){
				for(Object[] o : listfromQuery){
					Map<String, Object> mapResult = new HashMap();
					mapResult.put("id", o[0]);
					mapResult.put("friendly_group", o[1]);
					mapResult.put("friendly_name", o[2]);
					mapResult.put("friendly_tab", o[3]);
					mapResult.put("property_name", o[4]);
					mapResult.put("override_generate_prop_name", o[5]);
					mapResult.put("property_type", o[6]);
					mapResult.put("property_value", o[7]);
					result.add(mapResult);
				}
			}

			responseModel.setSuccess(true);
			responseModel.setMessage("Found system properties success");
			responseModel.setData(result);
			return responseModel;
		}catch (Exception e){
			LOG.error("===- Exception findByFriendlyGroup msg : "+e.getMessage()+" -===");
			responseModel.setSuccess(false);
			responseModel.setMessage("Can't found system properties msg "+e.getMessage());
			e.printStackTrace();
			return responseModel;
		}
	}
}

package com.community.service;

import org.springframework.http.ResponseEntity;

public interface UserProfileService {

	public String[] getFactoryCodeAndZoneCode(String userId);
	public Boolean authentication(String username,String password);
}

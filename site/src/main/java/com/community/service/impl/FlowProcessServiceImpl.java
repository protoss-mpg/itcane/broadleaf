package com.community.service.impl;

import java.nio.charset.Charset;
import java.util.Arrays;

import javax.annotation.Resource;

import org.broadleafcommerce.core.order.domain.Order;
import org.broadleafcommerce.core.order.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.community.service.FlowProcessService;
import com.protosstechnology.commons.util.JSONUtil;

@Service("flowProcessService")
public class FlowProcessServiceImpl implements FlowProcessService{
	
	@Resource(name = "blOrderService")
    protected OrderService orderService;
	
	@Value("${blc.server.flow.engine}")
	protected String flowEngineServer ;
	
	@Autowired
	RestTemplate restTemplate;

	@Override
	public ResponseEntity<String> createFlow(String orderNumber) {
		Order order = orderService.findOrderByOrderNumber(orderNumber);
		//Send to create flow : restTemplate
		String jsonString = JSONUtil.getJSONSerializer()
				.include("customer")
				.include("customer.emailAddress")
				.include("customer.firstName")
				.include("customer.lastName")
				.include("customer.username")
				.include("orderNumber")
				.exclude("*.*")
				.serialize(order);
		return postJsonData("/createProcess", jsonString);
	}
	
	public ResponseEntity<String> postJsonData(String urlParam, String jsonString) {
        String url = this.flowEngineServer + urlParam;
        //log.info(" request     URL:{}", url);
        //log.info(" jsonString json:{}", jsonString);
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");
        headers.setAcceptCharset(Arrays.asList(Charset.forName("UTF-8")));
    	HttpEntity<String> entity = new HttpEntity<String>(jsonString, headers);
    	return restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
    }

}

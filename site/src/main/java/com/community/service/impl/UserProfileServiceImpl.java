package com.community.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.community.service.UserProfileService;
import com.protosstechnology.commons.util.JSONUtil;

@Service("userProfileService")
public class UserProfileServiceImpl implements UserProfileService{
	
	@Value("${blc.server.user.profile}")
	protected String userProfileServer ;
	
	@Autowired
	RestTemplate restTemplate;

	@Override
	public String[] getFactoryCodeAndZoneCode(String userId) {
		ResponseEntity<String>  jsonData = getJsonData("/GetBPForGIS/IDcard/"+userId);
		String[] returnCode = new String[2];
		List<Map<String,Object>> resultLs = JSONUtil.fromJsonPath(jsonData.getBody(), List.class, "results");
		for(Map<String,Object> mapData:resultLs) {
			returnCode[0] = (String)mapData.get("cocde");
			returnCode[1] = (String)mapData.get("zone");
			break;
		}
		return returnCode;
	}
	
	public ResponseEntity<String> getJsonData(String urlParam) {
        String url = this.userProfileServer + urlParam;
    	return restTemplate.getForEntity(url, String.class);
    }

	@Override
	public Boolean authentication(String username, String password) {
		Boolean isAuthenSuccess = false;
		try {
			ResponseEntity<String>  jsonData = getJsonData("/GetBPForGIS/IDcard/"+username);
			List<Map<String,Object>> resultLs = JSONUtil.fromJsonPath(jsonData.getBody(), List.class, "results");
			for(Map<String,Object> mapData:resultLs) {
				String quotaNumber = (String)mapData.get("quota_no");
				if(String.valueOf(quotaNumber).equals(password)) {
					isAuthenSuccess = true;
					break;
				}
				
			}
		} catch (Exception e) {
			isAuthenSuccess = false;
		}
		return isAuthenSuccess;
	}

}

package com.community.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.broadleafcommerce.core.order.domain.Order;
import org.broadleafcommerce.core.order.service.type.OrderStatus;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Service;

import com.community.dao.OrderCustomDao;
import com.community.service.OrderCustomService;

@Service("blOrderCustomService")
@ManagedResource(objectName="org.broadleafcommerce:name=OrderCustomService", description="Order Custom Service", currencyTimeLimit=15)
public class OrderCustomServiceImpl implements OrderCustomService {
	
	private static final Log LOG = LogFactory.getLog(OrderCustomServiceImpl.class);
	
	@Resource(name = "blOrderCustomDao")
    protected OrderCustomDao orderCustomDao;

	@Override
	public List<Order> findOrdersForFactoryCode(String factoryCode, OrderStatus status) {
		LOG.debug("======================== blOrderCustomService findOrdersForFactoryCode factoryCode="+factoryCode);
		return orderCustomDao.readOrdersForCustomerName(factoryCode, status);
	}

	

}

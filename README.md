# MPG - MMARKET


## Software requires
* [Java]() -  version 8 or greater  
* [Apache Maven]() -  version  3.3.9  or greater


## Installation Guide 

* **[Prepare folder Asset & Temp Asset]()** <br/>
* **[Config database provider]()** <br/>
* **[Create Structure Database]()** <br/>
* **[Prepare Master Data]()** <br/>



[1]. Prepare folder Asset & Temp Asset <br/>
&#160;&#160;&#160;&#160;&#160;&#160;
 File location : admin/.../common.properties &
 site/.../common.properties
 ```sh
asset.server.file.system.path  = _____( asset folder)_____   
file.service.temp.file.base.directory = _____( temp asset folder)_____ 
```

[2]. Config database provider  <br/>
&#160;&#160;&#160;&#160;&#160;&#160;
File location : core/.../common-shared.properties
 ```sh
database.user= 
database.password=
database.driver=
database.url=
```
And set database mode in  file  : admin/.../default.properties<br/>
set mode to 'create'
 ```sh
blPU.hibernate.hbm2ddl.auto=create
blSecurePU.hibernate.hbm2ddl.auto=create
blEventPU.hibernate.hbm2ddl.auto=create
```
[3]. Create database structure <br/>

Build source code in root project type <br/>[mvn clean package install]()
```sh
$ ~/mmarket/ » mvn clean package install
```
 And Run project in admin side for Create Structure type<br/> [mvn spring-boot:run]()

```sh
$ ~/mmarket/admin » mvn spring-boot:run
```
[4]. Prepare master data <br/>
Run script sql in folder /sql for initial data
```sh
1_init_locale_and_currency.sql
2_update_catalog.sql
3_delete_product.sql
4_insert_admin_menu.sql
5_update_shipping_price.sql
6_insert_banner_config.sql
```
After create structure don't forget to change Database mode in admin/.../default.properties<br/> 
set mode to 'none' when run in next time
   ```sh
  blPU.hibernate.hbm2ddl.auto=none
  blSecurePU.hibernate.hbm2ddl.auto=none
  blEventPU.hibernate.hbm2ddl.auto=none
  ```                                                             

## Start Application

[1]. Run admin [mvn spring-boot:run]()
  ```sh
 $ ~/mmarket/admin » mvn spring-boot:run
 ```
[2]. Run Site [mvn spring-boot:run]()
  ```sh
 $ ~/mmarket/site » mvn spring-boot:run
 ```



## Build & Run Docker
* **[Build docker image]()** <br/>
* **[Run docker with properties config]()** <br/>
### <u>Admin</u>
##### Build docker image 
  ```sh
 $ ~/mmarket/admin/ » docker build -t mpg-mmarketplace-admin .
 ```
##### Run docker image 
change path to mount in docker container path to physical path (Folder asset, tempAsset, templateReport)
  ```sh
 $ ~/mmarket/admin/ » docker run 
                            -v /Users/dev2/Desktop/BL/AssetFile:/usr/local/mmarket/AssetFile 
                            -v /Users/dev2/Desktop/BL/TmpAssetFile:/usr/local/mmarket/TmpAssetFile 
                            -v /Users/dev2/Desktop/BL/TemplateReport:/usr/local/mmarket/TemplateReport 
                            -p 8081:8081 -p 8444:8444 
                            -p 8983:8983 
                            -it mpg-mmarketplace-admin
 ```
### <u>Site</u>
##### Build docker image 
  ```sh
 $ ~/mmarket/site/ » docker build -t mpg-mmarketplace-site .
 ```
##### Run docker image 
change path to mount in docker container path to physical path (Folder asset, tempAsset)
  ```sh
 $ ~/mmarket/site/ » docker run 
                            -v /Users/dev2/Desktop/BL/AssetFile:/usr/local/mmarket/AssetFile 
                            -v /Users/dev2/Desktop/BL/TmpAssetFile:/usr/local/mmarket/TmpAssetFile 
                            -p 8082:8082 
                            -p 8443:8443  
                            -it mpg-mmarketplace-site
 ```

## Property Config 
* **[Asset Location]()** 
* **[Solr Reindex]()** 
* **[Enable/Disable AOP to Clear Cache]()** 
* **[Path Template Report]()** 

##### <u>Asset Location</u>

File location : admin/.../common.properties &
 site/.../common.properties
```
asset.server.file.system.path  = _____( asset folder)_____   
file.service.temp.file.base.directory = _____( temp asset folder)_____ 
```


##### <u>Solr Reindex</u>
File location : admin/.../common.properties & site/.../common.properties
unit value is milliseconds (1000 = 1s)
```
solr.index.repeat.interval=
solr.index.start.delay=
```

##### <u>Enable / Disable AOP to Clear Cache</u>
this property is config to enable / disable clear cache image when 
upload media asset file location admin/.../common.properties (true = enable, false = disable)
```
enable.aop.clear.cache=true (default true)
```


##### <u>Path Template Report</u>
File location : admin/.../common.properties
```
path.template.order.summary.report = /usr/local/mmarket/TemplateReport/OrderSummaryReport.xlsx
path.template.order.summary.vender.report = /usr/local/mmarket/TemplateReport/OrderSummaryVenderReport.xlsx
path.template.best.seller.report  = /usr/local/mmarket/TemplateReport/BestSellerReport.xlsx
path.template.best.seller.vender.report  = /usr/local/mmarket/TemplateReport/BestSellerVenderReport.xlsx
path.template.poorly.sold.product.report = /usr/local/mmarket/TemplateReport/PoorlySoldProductReport.xlsx    
path.template.poorly.sold.product.vender.report  = /usr/local/mmarket/TemplateReport/PoorlySoldProductVenderReport.xlsx  
```


















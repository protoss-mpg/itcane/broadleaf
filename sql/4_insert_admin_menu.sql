
-- Create Role Vendor
INSERT INTO  blc_admin_role(admin_role_id,description,name)  VALUES (-8,'Vendor','ROLE_VENDOR');
INSERT INTO  blc_admin_role(admin_role_id,description,name)  VALUES (-9,'Factory Admin','ROLE_FACTORY_ADMIN');


-- Set Permission Read Order -> assignable
UPDATE blc_admin_permission SET is_friendly=true WHERE admin_permission_id=-12;
UPDATE blc_admin_permission SET is_friendly=true WHERE admin_permission_id=-13;

-- Add Permission of Vendor
INSERT INTO  blc_admin_permission(admin_permission_id,description,is_friendly,name,permission_type) VALUES (-300,'Read Vendor Category',true,'PERMISSION_READ_VENDOR_CATEGORY','READ');
INSERT INTO  blc_admin_permission(admin_permission_id,description,is_friendly,name,permission_type) VALUES (-301,'Maintain Vendor Category',true,'PERMISSION_ALL_VENDOR_CATEGORY','ALL');
INSERT INTO  blc_admin_permission(admin_permission_id,description,is_friendly,name,permission_type) VALUES (-302,'Read Vendor Assets',true,'PERMISSION_READ_VENDOR_ASSETS','READ');
INSERT INTO  blc_admin_permission(admin_permission_id,description,is_friendly,name,permission_type) VALUES (-303,'Maintain Vendor Assets',true,'PERMISSION_ALL_VENDOR_ASSETS','ALL');
INSERT INTO  blc_admin_permission(admin_permission_id,description,is_friendly,name,permission_type) VALUES (-304,'Read Vendor Product',true,'PERMISSION_READ_VENDOR_PRODUCT','READ');
INSERT INTO  blc_admin_permission(admin_permission_id,description,is_friendly,name,permission_type) VALUES (-305,'Maintain Vendor Product',true,'PERMISSION_ALL_VENDOR_PRODUCT','ALL');
INSERT INTO  blc_admin_permission(admin_permission_id,description,is_friendly,name,permission_type) VALUES (-306,'Read Order List Admin',true,'PERMISSION_READ_ORDER_ADMIN','READ');
INSERT INTO  blc_admin_permission(admin_permission_id,description,is_friendly,name,permission_type) VALUES (-307,'Read Order List Admin Factory',true,'PERMISSION_READ_ORDER_ADMIN_FACTORY','READ');
INSERT INTO  blc_admin_permission(admin_permission_id,description,is_friendly,name,permission_type) VALUES (-308,'Report Order Summary',true,'PERMISSION_REPORT_ORDER_SUMMARY','READ');
INSERT INTO  blc_admin_permission(admin_permission_id,description,is_friendly,name,permission_type) VALUES (-309,'Report Best Seller',true,'PERMISSION_REPORT_BEST_SELLER','READ');
INSERT INTO  blc_admin_permission(admin_permission_id,description,is_friendly,name,permission_type) VALUES (-310,'Report Poorly Sold Product',true,'PERMISSION_REPORT_POORLY_SOLD_PRODUCT','READ');
INSERT INTO  blc_admin_permission(admin_permission_id,description,is_friendly,name,permission_type) VALUES (-311,'Report Order Summary Vendor',true,'PERMISSION_REPORT_ORDER_SUMMARY_VENDOR','READ');
INSERT INTO  blc_admin_permission(admin_permission_id,description,is_friendly,name,permission_type) VALUES (-312,'Report Best Seller Vendor',true,'PERMISSION_REPORT_BEST_SELLER_VENDOR','READ');
INSERT INTO  blc_admin_permission(admin_permission_id,description,is_friendly,name,permission_type) VALUES (-313,'Report Poorly Sold Product Vendor',true,'PERMISSION_REPORT_POORLY_SOLD_PRODUCT_VENDOR','READ');

-- Set Role Permission
INSERT INTO  blc_admin_role_permission_xref(admin_role_id,admin_permission_id) VALUES (-1,-151);
INSERT INTO  blc_admin_role_permission_xref(admin_role_id,admin_permission_id) VALUES (-8,-300);
INSERT INTO  blc_admin_role_permission_xref(admin_role_id,admin_permission_id) VALUES (-8,-303);
INSERT INTO  blc_admin_role_permission_xref(admin_role_id,admin_permission_id) VALUES (-8,-305);
INSERT INTO  blc_admin_role_permission_xref(admin_role_id,admin_permission_id) VALUES (-8,-311);
INSERT INTO  blc_admin_role_permission_xref(admin_role_id,admin_permission_id) VALUES (-8,-312);
INSERT INTO  blc_admin_role_permission_xref(admin_role_id,admin_permission_id) VALUES (-8,-313);
INSERT INTO  blc_admin_role_permission_xref(admin_role_id,admin_permission_id) VALUES (-1,-306);
INSERT INTO  blc_admin_role_permission_xref(admin_role_id,admin_permission_id) VALUES (-1,-308);
INSERT INTO  blc_admin_role_permission_xref(admin_role_id,admin_permission_id) VALUES (-9,-307);
INSERT INTO  blc_admin_role_permission_xref(admin_role_id,admin_permission_id) VALUES (-9,-309);
INSERT INTO  blc_admin_role_permission_xref(admin_role_id,admin_permission_id) VALUES (-9,-310);
INSERT INTO  blc_admin_role_permission_xref(admin_role_id,admin_permission_id) VALUES (-9,-308);
INSERT INTO  blc_admin_role_permission_xref(admin_role_id,admin_permission_id) VALUES (-1,-309);
INSERT INTO  blc_admin_role_permission_xref(admin_role_id,admin_permission_id) VALUES (-1,-310);




-- Add Mudole 
INSERT INTO blc_admin_module (admin_module_id, display_order, icon, module_key, name) VALUES (-10, 800, 'fa fa-book', 'Order', 'Order');
INSERT INTO blc_admin_module (admin_module_id, display_order, icon, module_key, name) VALUES (-11, 900, 'fa fa-book', 'Vendor', 'Vendor');
INSERT INTO blc_admin_module (admin_module_id, display_order, icon, module_key, name) VALUES (-12, 1000, 'fa fa-file-excel-o', 'Report', 'Report');

-- Add Section
INSERT INTO blc_admin_section (admin_section_id, display_order, name, section_key, url, admin_module_id) VALUES (-1000, 1000, 'Order Listing', 'Order Listing Admin', '/requestOrder/listAdmin', -10);
INSERT INTO blc_admin_section (admin_section_id, display_order, name, section_key, url, admin_module_id) VALUES (-1001, 1000, 'Order Listing', 'Order Listing Factory', '/requestOrder/listFactoryAdmin', -10);
INSERT INTO blc_admin_section (admin_section_id, display_order, name, section_key, url, admin_module_id,ceiling_entity) VALUES (-1101, 1000, 'Categories', 'Categories', '/vendor/category', -11,'org.broadleafcommerce.core.catalog.domain.Category');
INSERT INTO blc_admin_section (admin_section_id, display_order, name, section_key, url, admin_module_id,ceiling_entity) VALUES (-1102, 1001, 'Media Assets', 'Media Assets', '/vendor/assets', -11,'org.broadleafcommerce.cms.file.domain.StaticAsset');
INSERT INTO blc_admin_section (admin_section_id, display_order, name, section_key, url, admin_module_id,ceiling_entity) VALUES (-1103, 1002, 'Product', 'Maintain Product', '/vendor/product', -11,'org.broadleafcommerce.core.catalog.domain.Product');
INSERT INTO blc_admin_section (admin_section_id, display_order, name, section_key, url, admin_module_id) VALUES (-1201, 1000, 'Order Summary Report', 'Order Summary Report', '/report/orderSummaryReport/listViewAdmin', -12);
INSERT INTO blc_admin_section (admin_section_id, display_order, name, section_key, url, admin_module_id) VALUES (-1202, 1000, 'Best Seller Report', 'Best Seller Report', '/report/bestSellerReport/listViewAdmin', -12);
INSERT INTO blc_admin_section (admin_section_id, display_order, name, section_key, url, admin_module_id) VALUES (-1203, 1000, 'Poorly Sold Product Report', 'Poorly Sold Product Report', '/report/poorlySoldProductReport/listViewAdmin', -12);
INSERT INTO blc_admin_section (admin_section_id, display_order, name, section_key, url, admin_module_id) VALUES (-1204, 1000, 'Order Summary Report', 'Order Summary Report Vendor', '/report/orderSummaryReport/listViewVendor', -12);
INSERT INTO blc_admin_section (admin_section_id, display_order, name, section_key, url, admin_module_id) VALUES (-1205, 1000, 'Best Seller Report', 'Best Seller Report Vendor', '/report/bestSellerReport/listViewVendor', -12);
INSERT INTO blc_admin_section (admin_section_id, display_order, name, section_key, url, admin_module_id) VALUES (-1206, 1000, 'Poorly Sold Product Report', 'Poorly Sold Product Report Vendor', '/report/poorlySoldProductReport/listViewVendor', -12);

-- Set Section Permission of Admin
INSERT INTO blc_admin_sec_perm_xref (admin_section_id, admin_permission_id) VALUES (-1000, -306);
INSERT INTO blc_admin_sec_perm_xref (admin_section_id, admin_permission_id) VALUES (-1001, -307);
INSERT INTO blc_admin_sec_perm_xref (admin_section_id, admin_permission_id) VALUES (-1101, -300);
INSERT INTO blc_admin_sec_perm_xref (admin_section_id, admin_permission_id) VALUES (-1102, -303);
INSERT INTO blc_admin_sec_perm_xref (admin_section_id, admin_permission_id) VALUES (-1103, -305);
INSERT INTO blc_admin_sec_perm_xref (admin_section_id, admin_permission_id) VALUES (-1201, -308);
INSERT INTO blc_admin_sec_perm_xref (admin_section_id, admin_permission_id) VALUES (-1201, -308);
INSERT INTO blc_admin_sec_perm_xref (admin_section_id, admin_permission_id) VALUES (-1202, -309);
INSERT INTO blc_admin_sec_perm_xref (admin_section_id, admin_permission_id) VALUES (-1203, -310);
INSERT INTO blc_admin_sec_perm_xref (admin_section_id, admin_permission_id) VALUES (-1204, -311);
INSERT INTO blc_admin_sec_perm_xref (admin_section_id, admin_permission_id) VALUES (-1205, -312);
INSERT INTO blc_admin_sec_perm_xref (admin_section_id, admin_permission_id) VALUES (-1206, -313);
-- Set Access Entity

INSERT INTO blc_admin_permission_entity(admin_permission_entity_id,ceiling_entity,admin_permission_id) VALUES(-10851,'org.broadleafcommerce.core.catalog.domain.Category',-300);
INSERT INTO blc_admin_permission_entity(admin_permission_entity_id,ceiling_entity,admin_permission_id) VALUES(-10852,'org.broadleafcommerce.core.catalog.domain.Category',-301);
INSERT INTO blc_admin_permission_entity(admin_permission_entity_id,ceiling_entity,admin_permission_id) VALUES(-10853,'org.broadleafcommerce.cms.file.domain.StaticAssetFolder',-302);
INSERT INTO blc_admin_permission_entity(admin_permission_entity_id,ceiling_entity,admin_permission_id) VALUES(-10854,'org.broadleafcommerce.cms.file.domain.StaticAsset',-302);
INSERT INTO blc_admin_permission_entity(admin_permission_entity_id,ceiling_entity,admin_permission_id) VALUES(-10855,'org.broadleafcommerce.cms.file.domain.StaticAssetFolder',-303);
INSERT INTO blc_admin_permission_entity(admin_permission_entity_id,ceiling_entity,admin_permission_id) VALUES(-10856,'org.broadleafcommerce.cms.file.domain.StaticAsset',-303);
INSERT INTO blc_admin_permission_entity(admin_permission_entity_id,ceiling_entity,admin_permission_id) VALUES(-10857,'org.broadleafcommerce.core.catalog.domain.Product',-304);
INSERT INTO blc_admin_permission_entity(admin_permission_entity_id,ceiling_entity,admin_permission_id) VALUES(-10858,'org.broadleafcommerce.core.catalog.domain.Product',-305);



